package com.febatis.gerenciadordepermisses;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

public class GerenciadorPermissoes {

    private static String TAG = "Permissões Febatis";
    public static int REQUEST_CODE = 2;

    //ACCESS_FINE_LOCATION
    public static boolean isPermissionGranted(Activity activity, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(permission)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"A permissão " + permission + " foi concedida");
                return true;
            } else {

                Log.v(TAG,"A permissão " + permission + " foi revogada");
                ActivityCompat.requestPermissions(activity, new String[]{permission}, REQUEST_CODE);
                return false;
            }
        }
        //Permissão é automaticamente concedida em Android SDK<23 (6.0)
        else {
            Log.v(TAG,"A permissão " + permission + " foi concedida");
            return true;
        }
    }

    public static boolean isReadStoragePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted1");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked1");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted1");
            return true;
        }
    }

    public static boolean isWriteStoragePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted2");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked2");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted2");
            return true;
        }
    }


    //BLUETOOTH_ADMIN
    public static boolean isBluetoothAdminPermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.BLUETOOTH_ADMIN)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"A permissão BLUETOOTH_ADMIN foi concedida");
                return true;
            } else {

                Log.v(TAG,"A permissão BLUETOOTH_ADMIN foi revogada");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.BLUETOOTH_ADMIN}, 2);
                return false;
            }
        }
        //Permissão é automaticamente concedida em Android SDK<23 (6.0)
        else {
            Log.v(TAG,"A permissão BLUETOOTH_ADMIN foi concedida");
            return true;
        }
    }

    //COARSE_LOCATION
    public static boolean isCoarseLocationPermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"A permissão ACCESS_COARSE_LOCATION foi concedida");
                return true;
            } else {

                Log.v(TAG,"A permissão ACCESS_COARSE_LOCATION foi revogada");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
                return false;
            }
        }
        //Permissão é automaticamente concedida em Android SDK<23 (6.0)
        else {
            Log.v(TAG,"A permissão ACCESS_COARSE_LOCATION foi concedida");
            return true;
        }
    }

    //ACCESS_FINE_LOCATION
    public static boolean isFineLocationPermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"A permissão ACCESS_FINE_LOCATION foi concedida");
                return true;
            } else {

                Log.v(TAG,"A permissão ACCESS_FINE_LOCATION foi revogada");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2);
                return false;
            }
        }
        //Permissão é automaticamente concedida em Android SDK<23 (6.0)
        else {
            Log.v(TAG,"A permissão ACCESS_FINE_LOCATION foi concedida");
            return true;
        }
    }


}
