# LEIA-ME #

Esse arquivo contém informações importantes sobre o projeto, desde detalhes a como gerar um apk a partir dele. 

### Rodando Pipeline ###

## Branches View ##

Branches > ... > Run pipeline for a branch

## Commit View ##

Run pipeline

### Requisitos ###

* Android Studio >3.0 (Para desenvolver)
* Android >5 (Lollipop) (Para usar)

### Como obter o projeto para desenvolvimento? ###

* TortoiseGit
Abra o cmd ou terminal na pasta desejada e execute:
git clone https://{usuario}@bitbucket.org/{usuario}/pdfc-app.git
Sendo que {usuario} é um usuario com permissão de download do repositorio.

* Android Studio
Abra o android studio e vá em settings e informe o path do git.
Na tela inicial do android studio, vá em import projet from control version e informe a seguinte url:
https://{usuario}@bitbucket.org/{usuario}/pdfc-app.git
Sendo que {usuario} é um usuario com permissão de download do repositorio.

## Criadores ##
Arthur 
Lucas Batista
