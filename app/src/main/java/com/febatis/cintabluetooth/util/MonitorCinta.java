package com.febatis.cintabluetooth.util;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;

import com.febatis.cintabluetooth.cinta.CintaClient2;
import com.febatis.gerenciadordepermisses.GerenciadorPermissoes;

/**
 * Criado por lfeba em 28/05/2018.
 */
public final class MonitorCinta {

    public static void configBluetoothAdapter(Activity activity, int REQUEST_ENABLE_BT) {
        boolean permissao = GerenciadorPermissoes.isBluetoothAdminPermissionGranted(activity);

        if(permissao) {

            GerenciadorBroadcast.dispositivoPareadoId = GerenciadorSP.obterCintaPareada(activity);

            BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            BluetoothManager btManager = (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);

            if(btAdapter.isEnabled()) {
                CintaClient2 cintaClient = new CintaClient2(activity, btAdapter);
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }

        } else {
            return;
        }
    }
}
