package com.febatis.cintabluetooth.util;

import android.content.Context;
import android.content.Intent;

/**
 * Criado por lfeba em 08/04/2018.
 */

public class GerenciadorBroadcast {

    public static final int REQUEST_ENABLE_BT = 1;

    //"46CD801E"
    public static String dispositivoPareadoId = "";
    public static String dispositivoPareadoMac = "";

    public static final String INTENT_FILTER_BC_BLUETOOTH = "Atualizacao_Bluetooth";
    public static final String BLUETOOTH_ACAO = "Bluetooth acao";
    public static final String BLUETOOTH_MSG = "Bluetooth msg";
    public static final String BLUETOOTH_ADD_ITEM = "Bluetooth add item";

    public static final String BLUETOOTH_ENCONTRADO = "mandarDispositivoEncontrado";
    public static final String BLUETOOTH_POLAR_ENCONTRADO = "mandarDispositivoPolarEncontrado";
    public static final String BLUETOOTH_PAREADO_MSG = "mandarDispositivoPareadoMsg";


    public static final String BLUETOOTH_CINTA_ID = "cinta_id";
    public static final String BLUETOOTH_ENDERECO_MAC = "endereco_mac";

    public static void mandarMensagem(Context context, String msg) {
        Intent intent = new Intent(INTENT_FILTER_BC_BLUETOOTH);
        intent.putExtra(BLUETOOTH_ACAO, BLUETOOTH_MSG);
        intent.putExtra(BLUETOOTH_MSG, msg);
        context.sendBroadcast(intent);
    }

    public static void mandarAddItem(Context context, String item) {
        Intent intent = new Intent(INTENT_FILTER_BC_BLUETOOTH);
        intent.putExtra(BLUETOOTH_ACAO, BLUETOOTH_ADD_ITEM);
        intent.putExtra(BLUETOOTH_ADD_ITEM, item);
        context.sendBroadcast(intent);
    }

    public static void mandarDispositivoEncontrado(Context context, String msg) {
        Intent intent = new Intent(INTENT_FILTER_BC_BLUETOOTH);
        intent.putExtra(BLUETOOTH_ACAO, BLUETOOTH_ENCONTRADO);
        intent.putExtra(BLUETOOTH_MSG, msg);
        context.sendBroadcast(intent);
    }

    public static void mandarDispositivoPolarEncontrado(Context context, String msg, String enderecoMac) {
        Intent intent = new Intent(INTENT_FILTER_BC_BLUETOOTH);
        intent.putExtra(BLUETOOTH_ACAO, BLUETOOTH_POLAR_ENCONTRADO);
        intent.putExtra(BLUETOOTH_CINTA_ID, msg);
        intent.putExtra(BLUETOOTH_ENDERECO_MAC, enderecoMac);
        context.sendBroadcast(intent);
    }

    public static void mandarDispositivoPareadoMsg(Context context, String msg) {
        Intent intent = new Intent(INTENT_FILTER_BC_BLUETOOTH);
        intent.putExtra(BLUETOOTH_ACAO, BLUETOOTH_PAREADO_MSG);
        intent.putExtra(BLUETOOTH_MSG, msg);
        context.sendBroadcast(intent);
    }

}
