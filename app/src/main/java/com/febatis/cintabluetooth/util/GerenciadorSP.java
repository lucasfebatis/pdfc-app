package com.febatis.cintabluetooth.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Criado por lfeba em 27/05/2018.
 */
public final class GerenciadorSP {

    private final static String PREFS_HOME = "prefs_com.febatis.cintabluetooth";
    private final static String PREFS_TEMA = "tema";
    private final static String PREFS_SE_PRIMEIRO_ACESSO = "se_primeiro_acesso";
    private final static String PREFS_CINTA_PAREADA = "cinta_pareada";


    public static String obterTema(Context context) {

        // Restore preferences
        SharedPreferences settings = context.getSharedPreferences(PREFS_HOME, 0);
        return settings.getString(PREFS_TEMA, "");
    }

    public static void definirTema(Context context, String tema) {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = context.getSharedPreferences(PREFS_HOME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_TEMA, tema);

        // Commit or apply for background commit the edits!
        editor.apply();
    }


    public static boolean obterSePrimeiroAcesso(Context context) {

        SharedPreferences settings = context.getSharedPreferences(PREFS_HOME, 0);
        return settings.getBoolean(PREFS_SE_PRIMEIRO_ACESSO, true);

    }

    public static void definirSePrimeiroAcesso(Context context, boolean sePrimeiroAcesso) {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = context.getSharedPreferences(PREFS_HOME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(PREFS_SE_PRIMEIRO_ACESSO, sePrimeiroAcesso);

        // Commit or apply for background commit the edits!
        editor.apply();
    }

    public static String obterCintaPareada(Context context) {

        SharedPreferences settings = context.getSharedPreferences(PREFS_HOME, 0);
        return settings.getString(PREFS_CINTA_PAREADA, "");

    }

    public static void definirCintaPareada(Context context, String cintaPareada) {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = context.getSharedPreferences(PREFS_HOME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_CINTA_PAREADA, cintaPareada);

        // Commit or apply for background commit the edits!
        editor.apply();
    }

}
