package com.febatis.cintabluetooth.util;

/**
 * Criado por lfeba em 13/05/2018.
 */
public class MatematicaUtil {

    public static double arredondar(double val) {
        double resto = val % 1;
        if (resto < 0.25) {
            val = val - resto;
        } else {
            if (resto > 0.75) {
                val = val - resto + 1;
            } else {
                val = val - resto + 0.5;
            }
        }

        return val;

    }

    public static String velocidadeToString(double velocidade) {
        return velocidade + " Km/h";
    }

    public static String frequenciaToString(int frequencia) {
        return frequencia + "BPM";
    }

    public static double stringToVelocicdade(String velocidade) {
        try {
            String v = velocidade.split("Km/h")[0];
            return Double.valueOf(v);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int stringToFrequencia(String frequencia) {

        if (frequencia.contains("BPM")) {
            try {
                String v = frequencia.split(" BPM")[0];
                return Integer.valueOf(v);
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        } else if (frequencia.contains("Frequência Cardíaca: ")){
            try {
                String v = frequencia.split("Frequência Cardíaca: ")[1];
                return Integer.valueOf(v);
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            return 0;
        }
    }

}
