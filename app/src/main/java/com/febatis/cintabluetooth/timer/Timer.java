package com.febatis.cintabluetooth.timer;

import android.os.Handler;

import java.util.Locale;

/**
 * Criado por lfeba em 13/05/2018.
 */
public class Timer {

    public interface OnCustomEventListener {
        void onEvent(String tempo);
    }

    private Handler mHandler = new Handler();
    private OnCustomEventListener customEventListener;

    private long initialTime;
    private static boolean isRunning;
    private static final long MILLIS_IN_SEC = 1000L;
    private static final int SECS_IN_MIN = 60;

    public Timer(OnCustomEventListener customEventListener) {
        this.customEventListener = customEventListener;
    }

    private Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {
            if (isRunning) {
                long seconds = (System.currentTimeMillis() - initialTime) / MILLIS_IN_SEC;
                customEventListener.onEvent(String.format(new Locale("pt","BR"), "%02d:%02d", seconds / SECS_IN_MIN, seconds % SECS_IN_MIN));
                mHandler.postDelayed(mHandlerTask, MILLIS_IN_SEC);
            }
        }
    };

    public void startRepeatingTask() {
        isRunning = true;
        initialTime = System.currentTimeMillis();
        mHandlerTask.run();
    }

    public void stopRepeatingTask() {
        isRunning = false;
        mHandler.removeCallbacks(mHandlerTask);
    }


}
