package com.febatis.cintabluetooth.ui.activity;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.base.BaseActivity;
import com.febatis.cintabluetooth.cinta.CintaClient2;
import com.febatis.cintabluetooth.model.CintaDeviceItem;
import com.febatis.cintabluetooth.model.CintaPareadaDeviceItem;
import com.febatis.cintabluetooth.ui.adapter.CintaDeviceAdapter;
import com.febatis.cintabluetooth.ui.adapter.CintaPareadaDeviceAdapter;
import com.febatis.cintabluetooth.util.GerenciadorSP;
import com.febatis.cintabluetooth.util.MonitorCinta;
import com.febatis.gerenciadordepermisses.GerenciadorPermissoes;

import java.util.ArrayList;
import java.util.Objects;

import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_ACAO;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_ADD_ITEM;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_CINTA_ID;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_ENDERECO_MAC;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_MSG;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_PAREADO_MSG;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_POLAR_ENCONTRADO;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.dispositivoPareadoId;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.dispositivoPareadoMac;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.INTENT_FILTER_BC_BLUETOOTH;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.REQUEST_ENABLE_BT;

public class ConexaoActivity extends BaseActivity {

    RecyclerView rv_cinta_pareada;
    CintaPareadaDeviceAdapter mAdapterCintaPareada;
    CintaPareadaDeviceItem myData;

    RecyclerView rv_dispositivos;
    CintaDeviceAdapter mAdapterCintaDevice;
    ArrayList<CintaDeviceItem> myDataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conexao);

        //configBluetoothAdapter();
        MonitorCinta.configBluetoothAdapter(this, REQUEST_ENABLE_BT);

        configActivity();
        configCintaPareadaView();
        configDispositivosView();

    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(bc_bluetooth, new IntentFilter(INTENT_FILTER_BC_BLUETOOTH));
    }

    @Override
    protected void onResume() {
        super.onResume();
        //GerenciadorBroadcast.mandarDispositivoPolarEncontrado(this, "46CD801E", "00:19:B9:FB:E2:58");
        //GerenciadorBroadcast.mandarDispositivoPolarEncontrado(this, "47CD801E", "00:19:B9:FB:E2:58");
        //GerenciadorBroadcast.mandarDispositivoPolarEncontrado(this, "46CD801E", "00:19:B9:FB:E2:58");
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(bc_bluetooth);
    }

    public void configBluetoothAdapter() {

        boolean permissao = GerenciadorPermissoes.isBluetoothAdminPermissionGranted(this);

        if(permissao) {

            BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

            if(btAdapter.isEnabled()) {
                CintaClient2 cintaClient = new CintaClient2(this, btAdapter);
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }

        } else {
            mostrarSnackBar("Sem permissão para acessar bluetooth", Snackbar.LENGTH_SHORT);
        }
    }

    private void configActivity() {

        boolean isIntro = getIntent().getBooleanExtra("isIntro", false);

        if (isIntro) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(false);

            setFloatButtonListener(onClick_FloatButton);
        }


    }

    private void configCintaPareadaView() {

        RecyclerView.LayoutManager mLayoutManager;

        rv_cinta_pareada = findViewById(R.id.dispositivoPareado);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        rv_cinta_pareada.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        rv_cinta_pareada.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapterCintaPareada = new CintaPareadaDeviceAdapter(myData, onClick_CintaPareada);
        rv_cinta_pareada.setAdapter(mAdapterCintaPareada);

    }

    private void configDispositivosView() {

        myDataset = new ArrayList<>();

        /*
        myDataset.add(new CintaDeviceItem("6786","ytr67"));
        myDataset.add(new CintaDeviceItem("46CD801E","00:19:B9:FB:E2:58"));
        myDataset.add(new CintaDeviceItem("7587657","hfjhgfjgf"));
        myDataset.add(new CintaDeviceItem("7576","86554564864"));
        myDataset.add(new CintaDeviceItem("54756","utguiygkygk"));
        myDataset.add(new CintaDeviceItem("8964","uftrutdfjtrug"));
        myDataset.add(new CintaDeviceItem("7233","re6rdyfvtfrtyhu8u"));
        myDataset.add(new CintaDeviceItem("84235","awsfr678u7tsyuy"));
        myDataset.add(new CintaDeviceItem("84235","awsfr678u7tsyuy"));
        myDataset.add(new CintaDeviceItem("84235","awsfr678u7tsyuy"));
        myDataset.add(new CintaDeviceItem("84235","awsfr678u7tsyuy"));
        myDataset.add(new CintaDeviceItem("84235","Ultimo Item"));
        */

        RecyclerView.LayoutManager mLayoutManager;

        rv_dispositivos = findViewById(R.id.listaDispositivos);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //rv_dispositivos.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        rv_dispositivos.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapterCintaDevice = new CintaDeviceAdapter(rv_dispositivos, myDataset, onClick_Dispositivos);
        rv_dispositivos.setAdapter(mAdapterCintaDevice);

    }


    //Eventos
    CintaPareadaDeviceAdapter.OnItemClickListener onClick_CintaPareada = menuItem -> {
        myData = null;
        mAdapterCintaPareada.atualizarLista(null);
        dispositivoPareadoId = "";
        dispositivoPareadoMac = "";
    };

    CintaDeviceAdapter.OnItemClickListener onClick_Dispositivos = menuItem -> {
        dispositivoPareadoId = menuItem.getIdentificador();
        GerenciadorSP.definirCintaPareada(getApplicationContext(), dispositivoPareadoId);
        dispositivoPareadoMac = menuItem.getEnderecoMac();
        //GerenciadorBroadcast.mandarDispositivoPareadoMsg(this, "0 BPM");
    };

    View.OnClickListener onClick_FloatButton = view -> {

        if(myData != null) {
        //if(true) {
            Intent intent = new Intent(getApplicationContext(), CadastroPerfilActivity.class);
            intent.putExtra("isIntro",true);
            startActivity(intent);
        } else {
            mostrarSnackBar("Você precisa parear uma cinta antes de continuar", Snackbar.LENGTH_SHORT);
        }


    };


    //Broadcast
    BroadcastReceiver bc_bluetooth = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String acao = intent.getStringExtra(BLUETOOTH_ACAO);
            String msg;

            String cintaId;
            String enderecoMac;

            switch (acao) {
                case BLUETOOTH_MSG:
                    msg = intent.getStringExtra(BLUETOOTH_MSG);
                    break;
                case BLUETOOTH_ADD_ITEM:
                    String add_item = intent.getStringExtra(BLUETOOTH_ADD_ITEM);
                    break;
                case BLUETOOTH_POLAR_ENCONTRADO:
                cintaId = intent.getStringExtra(BLUETOOTH_CINTA_ID);
                enderecoMac = intent.getStringExtra(BLUETOOTH_ENDERECO_MAC);

                if(!myDataset.contains(new CintaDeviceItem(cintaId,enderecoMac))) {
                    myDataset.add(new CintaDeviceItem(cintaId,enderecoMac));
                    mAdapterCintaDevice.atualizarLista(myDataset);
                }


                break;
                case BLUETOOTH_PAREADO_MSG:
                    msg = intent.getStringExtra(BLUETOOTH_MSG);

                    myData = new CintaPareadaDeviceItem(dispositivoPareadoId, dispositivoPareadoMac, msg);
                    mAdapterCintaPareada.atualizarLista(myData);
                    break;
            }
        }
    };
}
