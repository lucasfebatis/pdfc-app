package com.febatis.cintabluetooth.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.model.HistoricoItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class HistoricoAdapter extends RecyclerView.Adapter<HistoricoAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onClick(HistoricoItem historicoItem);
    }

    private ArrayList<HistoricoItem> mDataset;
    private OnItemClickListener onItemClickListener;

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvNome;
        TextView tvKm;
        TextView tvPdfc;
        TextView tvData;

        TextView tvMsg;

        ViewHolder(View v) {
            super(v);
            tvNome = v.findViewById(R.id.tv_nome);
            tvKm = v.findViewById(R.id.tv_km);
            tvPdfc = v.findViewById(R.id.tv_pdfc);
            tvData = v.findViewById(R.id.tv_data);

            tvMsg = v.findViewById(R.id.tv_msg);
        }
    }

    public HistoricoAdapter(ArrayList<HistoricoItem> myDataset, OnItemClickListener onItemClickListener) {
        mDataset = myDataset;
        this.onItemClickListener = onItemClickListener;

    }

    @NonNull
    @Override
    public HistoricoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_historico, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (mDataset != null && !mDataset.isEmpty()) {
            holder.tvNome.setText(mDataset.get(position).getNome());
            holder.tvPdfc.setText("PDFC: " + mDataset.get(position).getPdfc());
            holder.tvKm.setText("Distância Percorrida: " + mDataset.get(position).getKmPercorrido() + " km");

            String data;

            SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy '\nàs' HH:mm:ss", new Locale("pt","br"));
            data = dt1.format(mDataset.get(position).getData());

            holder.tvData.setText(data);

            holder.itemView.setOnClickListener(view -> onItemClickListener.onClick(mDataset.get(holder.getAdapterPosition())));
        } else {
            holder.tvMsg.setText("Você ainda não realizou testes.\nQuando fizer você poderá consulta-los aqui");
        }

    }

    @Override
    public int getItemCount() {
        return mDataset == null ? 0 : mDataset.size();
    }

    public void atualizarLista(ArrayList<HistoricoItem> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

}