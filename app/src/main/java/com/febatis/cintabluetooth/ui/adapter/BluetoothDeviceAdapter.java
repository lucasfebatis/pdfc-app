package com.febatis.cintabluetooth.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.model.BluetoothDeviceItem;

import java.util.ArrayList;

public class BluetoothDeviceAdapter extends RecyclerView.Adapter<BluetoothDeviceAdapter.ViewHolder> {

    private final int VIEW_TYPE_SEM_ITENS = -10;
    private ArrayList<BluetoothDeviceItem> mDataset;

    static class ViewHolder extends RecyclerView.ViewHolder {

         TextView tvIdentificador;
         TextView tvStatus;

         ViewHolder(View v) {
            super(v);
            tvIdentificador = v.findViewById(R.id.device_identificador);
            tvStatus = v.findViewById(R.id.device_status);
        }
    }

    public BluetoothDeviceAdapter(RecyclerView recyclerView, ArrayList<BluetoothDeviceItem> myDataset) {
        mDataset = myDataset;

        if(mDataset == null || mDataset.isEmpty()) {
            onCreateViewHolder(recyclerView, VIEW_TYPE_SEM_ITENS);
        }

    }

    @Override
    public BluetoothDeviceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;

        if(viewType != VIEW_TYPE_SEM_ITENS)
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bluetooth_device, parent, false);
        else
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sem_bluetooth_device, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if(holder.tvIdentificador != null) {
            holder.tvIdentificador.setText(mDataset.get(position).getIdentificador());
            holder.tvStatus.setText(mDataset.get(position).getStatus());
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    public void atualizarLista(ArrayList<BluetoothDeviceItem> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

}