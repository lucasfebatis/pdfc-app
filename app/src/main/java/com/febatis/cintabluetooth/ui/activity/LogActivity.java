package com.febatis.cintabluetooth.ui.activity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.base.BaseActivity;
import com.febatis.cintabluetooth.cinta.CintaClient;
import com.febatis.cintabluetooth.matematica.JamaTestes2;
import com.febatis.cintabluetooth.model.BluetoothDeviceItem;
import com.febatis.cintabluetooth.testes.Testes;
import com.febatis.cintabluetooth.ui.adapter.BluetoothDeviceAdapter;
import com.febatis.cintabluetooth.util.MonitorCinta;
import com.febatis.gerenciadordepermisses.GerenciadorPermissoes;

import java.util.ArrayList;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_ACAO;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_ADD_ITEM;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_MSG;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.INTENT_FILTER_BC_BLUETOOTH;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.REQUEST_ENABLE_BT;

public class LogActivity extends BaseActivity {

    /*
    public static final int REQUEST_ENABLE_BT = 1;
    public static final String INTENT_FILTER_BC_BLUETOOTH = "Atualizacao_Bluetooth";
    public static final String BLUETOOTH_ACAO = "Bluetooth acao";
    public static final String BLUETOOTH_MSG = "Bluetooth msg";
    public static final String BLUETOOTH_ADD_ITEM = "Bluetooth add item";
    */

    View root;
    ArrayList<BluetoothDeviceItem> itens;
    BluetoothDeviceAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        root = findViewById(R.id.root);

        /*
        JamaTestes.exemploMultiplicacao();
        JamaTestes.exemploObterResidual();
        JamaTestes.exemploInverterMatriz();
        */

        double distancia = 3.6; //(em km)
        double tempo = 1800; //(em sec)
        double velocidadeInicial = ((distancia * 3600)/tempo) * 0.65; //Arredondar

        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Arthur.class.getSimpleName(), Testes.Arthur.velocidades, Testes.Arthur.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Batista.class.getSimpleName(), Testes.Batista.velocidades, Testes.Batista.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Cesar.class.getSimpleName(), Testes.Cesar.velocidades, Testes.Cesar.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Diogo.class.getSimpleName(), Testes.Diogo.velocidades, Testes.Diogo.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Felipe.class.getSimpleName(), Testes.Felipe.velocidades, Testes.Felipe.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Gustavo.class.getSimpleName(), Testes.Gustavo.velocidades, Testes.Gustavo.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Larissa.class.getSimpleName(), Testes.Larissa.velocidades, Testes.Larissa.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Mathias.class.getSimpleName(), Testes.Mathias.velocidades, Testes.Mathias.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Mauricio.class.getSimpleName(), Testes.Mauricio.velocidades, Testes.Mauricio.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Rafael.class.getSimpleName(), Testes.Rafael.velocidades, Testes.Rafael.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Richie.class.getSimpleName(), Testes.Richie.velocidades, Testes.Richie.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Rodrigo.class.getSimpleName(), Testes.Rodrigo.velocidades, Testes.Rodrigo.frequencias));
        Log.e("LogActivity: ", JamaTestes2.gerarTeste(Testes.Vanessa.class.getSimpleName(), Testes.Vanessa.velocidades, Testes.Vanessa.frequencias));
        //JamaTestes.gerarTeste2();

        if(GerenciadorPermissoes.isPermissionGranted(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            initActivity();
        }

    }

    void initActivity() {
        itens = new ArrayList<>();

        //configBluetoothAdapter();
        MonitorCinta.configBluetoothAdapter(this, REQUEST_ENABLE_BT);
        configRecyclerView(itens);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == GerenciadorPermissoes.REQUEST_CODE) {

            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        initActivity();
                    } else {
                        Snackbar.make(root, "Sem permissão", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }

        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(bc_bluetooth, new IntentFilter(INTENT_FILTER_BC_BLUETOOTH));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(bc_bluetooth);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                if (resultCode == RESULT_OK) {
                    configBluetoothAdapter();
                } else {
                    Snackbar.make(root, "Bluetooth desativado", Snackbar.LENGTH_SHORT).show();
                }
                break;
        }
    }


    public void configBluetoothAdapter() {
        boolean permissao = GerenciadorPermissoes.isBluetoothAdminPermissionGranted(this);

        if(permissao) {
            Snackbar.make(root, "Sim", Snackbar.LENGTH_SHORT).show();

            BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

            if(btAdapter.isEnabled()) {
                CintaClient cintaClient = new CintaClient(this, btAdapter);
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }

        } else {
            Snackbar.make(root, "Não", Snackbar.LENGTH_SHORT).show();
        }
    }

    public void configRecyclerView(ArrayList<BluetoothDeviceItem> myDataset) {

        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;

        mRecyclerView = findViewById(R.id.listaDispositivos);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new BluetoothDeviceAdapter(mRecyclerView, myDataset);
        mRecyclerView.setAdapter(mAdapter);

    }


    BroadcastReceiver bc_bluetooth = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String acao = intent.getStringExtra(BLUETOOTH_ACAO);

            switch (acao) {
                case BLUETOOTH_MSG:
                    String msg = intent.getStringExtra(BLUETOOTH_MSG);
                    Snackbar.make(root, msg, Snackbar.LENGTH_SHORT).show();
                    break;
                case BLUETOOTH_ADD_ITEM:
                    String add_item = intent.getStringExtra(BLUETOOTH_ADD_ITEM);
                    findViewById(R.id.tv_sem_itens).setVisibility(View.GONE);
                    itens.add(new BluetoothDeviceItem(add_item, "Descoberto"));
                    mAdapter.atualizarLista(itens);
                    break;
            }
        }
    };



}
