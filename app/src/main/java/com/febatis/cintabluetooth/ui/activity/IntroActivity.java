package com.febatis.cintabluetooth.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.util.GerenciadorSP;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import agency.tango.materialintroscreen.MessageButtonBehaviour;
import agency.tango.materialintroscreen.SlideFragmentBuilder;

public class IntroActivity extends MaterialIntroActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        hideBackButton();

        boolean o = GerenciadorSP.obterSePrimeiroAcesso(getApplicationContext());

        if(!o) {
        //if(false) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }


        addSlide(
                new SlideFragmentBuilder()
                        .backgroundColor(R.color.colorAccent_Claro)
                        .buttonsColor(R.color.colorPrimary_Claro)
                        .title("PDFC")
                        .description("Descrição")
                        .image(R.mipmap.ic_launcher_round)
                        .build()
        );
        
        String[] permissoes = new String[] { Manifest.permission.ACCESS_COARSE_LOCATION };

        addSlide(new SlideFragmentBuilder()
                        .backgroundColor(R.color.colorAccent_Claro)
                        .buttonsColor(R.color.colorPrimary_Claro)
                        .title("Permissões necessarias")
                        .neededPermissions(permissoes)
                        .description("A permissão de localização é necessaria para acessar os dados dos sensores")
                        .image(R.mipmap.ic_launcher_round)
                        .build());
        
        addSlide(new SlideFragmentBuilder()
                        .backgroundColor(R.color.colorAccent_Claro)
                        .buttonsColor(R.color.colorPrimary_Claro)
                        .title("Vamos para os testes")
                        .description("Antes de começar é importante que você já tenha realizado o pré teste, pois os dados obtidos nele serão necessarios para definir sua velocidade inicial neste teste")
                        .image(R.mipmap.ic_launcher_round)
                        .build(),
                new MessageButtonBehaviour(v -> {

                    Intent intent = new Intent(getApplicationContext(), ConexaoActivity.class);
                    intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                    intent.putExtra("isIntro",true);
                    startActivity( intent );
                    finish();
                }, "Iniciar Testes"));


    }

    @Override
    public void setBackButtonVisible() {

        ImageButton backButton =  findViewById(agency.tango.materialintroscreen.R.id.button_back);
        ImageButton nextButton =  findViewById(agency.tango.materialintroscreen.R.id.button_next);
        ImageButton skipButton =  findViewById(agency.tango.materialintroscreen.R.id.button_skip);

        backButton.setVisibility(View.INVISIBLE);
        nextButton.setVisibility(View.INVISIBLE);
        skipButton.setVisibility(View.INVISIBLE);

    }

    @Override
    public void setSkipButtonVisible() {

        ImageButton backButton =  findViewById(agency.tango.materialintroscreen.R.id.button_back);
        ImageButton nextButton =  findViewById(agency.tango.materialintroscreen.R.id.button_next);
        ImageButton skipButton =  findViewById(agency.tango.materialintroscreen.R.id.button_skip);

        backButton.setVisibility(View.INVISIBLE);
        nextButton.setVisibility(View.INVISIBLE);
        skipButton.setVisibility(View.INVISIBLE);

    }
}
