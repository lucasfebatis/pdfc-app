package com.febatis.cintabluetooth.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.model.ResultadoItem;

import java.util.ArrayList;

public class ResultadoAdapter extends RecyclerView.Adapter<ResultadoAdapter.ViewHolder> {

    private final int VIEW_TYPE_SEM_ITENS = -10;
    private ArrayList<ResultadoItem> mDataset;

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvValor;

        ViewHolder(View v) {
            super(v);
            tvValor = v.findViewById(R.id.tv_valor);
        }
    }

    public ResultadoAdapter(RecyclerView recyclerView, ArrayList<ResultadoItem> myDataset) {
        mDataset = myDataset;

        if (mDataset == null || mDataset.isEmpty()) {
            onCreateViewHolder(recyclerView, VIEW_TYPE_SEM_ITENS);
        }

    }

    @NonNull
    @Override
    public ResultadoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_resultado, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (holder.tvValor != null) {
            holder.tvValor.setText(mDataset.get(position).getValor());
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    public void atualizarLista(ArrayList<ResultadoItem> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

}