package com.febatis.cintabluetooth.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.base.BaseActivity;
import com.febatis.cintabluetooth.model.Usuario;
import com.febatis.cintabluetooth.realm.RealmDAL;
import com.febatis.cintabluetooth.util.GerenciadorSP;
import com.febatis.cintabluetooth.util.MaskEditUtil;

import java.util.Objects;

import io.realm.Realm;

import static com.febatis.cintabluetooth.ui.activity.MedicaoActivity.isCicloRapido;

public class PreTesteActivity extends BaseActivity {

    EditText et_nome;
    EditText et_distancia;
    EditText et_tempo;
    CheckBox check_ciclo_rapido;
    Button btn_avancar;

    RealmDAL realmDAL;

    Usuario usuario;
    Usuario usuario_cadastro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_teste);

        initViews();
        initUsuario();
        configActivity();

    }

    private void configActivity() {

        boolean isIntro = getIntent().getBooleanExtra("isIntro", false);

        if (isIntro) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(false);

            btn_avancar.setOnClickListener(onClick_Avancar_MainActivity);
            check_ciclo_rapido.setVisibility(View.GONE);
            usuario_cadastro = getIntent().getParcelableExtra("usuario");
            et_nome.setText(usuario_cadastro.getNome());
        }


    }

    private void initUsuario() {

        Realm.init(getApplicationContext());
        realmDAL = new RealmDAL(this);

        try {
            usuario = (Usuario) realmDAL.obterRegistro(Usuario.class, 0);
            et_nome.setText(usuario.getNome());
            et_distancia.setText(usuario.getDistancia());
            et_tempo.setText(usuario.getTempo());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void initViews() {

        et_nome = findViewById(R.id.et_nome);
        et_distancia = findViewById(R.id.et_distancia);

        et_tempo = findViewById(R.id.et_tempo);
        et_tempo.addTextChangedListener(MaskEditUtil.mask(et_tempo, "##:##:##"));

        check_ciclo_rapido = findViewById(R.id.check_ciclo_rapido);
        check_ciclo_rapido.setOnCheckedChangeListener(onChange_CicloRapido);

        btn_avancar = findViewById(R.id.btn_avancar);
        btn_avancar.setOnClickListener(onClick_Avancar_MedicaoActivity);
    }

    //Eventos
    CompoundButton.OnCheckedChangeListener onChange_CicloRapido = (compoundButton, b) ->
            isCicloRapido = check_ciclo_rapido.isChecked();


    private View.OnClickListener onClick_Avancar_MedicaoActivity = view -> {

        try {

            //Validar Tempo
            String[] toSec = et_tempo.getText().toString().split(":");
            if(toSec.length != 3) {
                mostrarSnackBar("O campo tempo deve estar neste formato 00:00:00", Snackbar.LENGTH_LONG);
                return;
            } else if(toSec[0].equals("00") && toSec[1].equals("00")) {
                mostrarSnackBar("Seu teste deve ter uma duração minima de 1 minuto", Snackbar.LENGTH_LONG);
                return;
            } else if(toSec[2].length() == 1) {
                mostrarSnackBar("O campo tempo deve estar neste formato 00:00:00", Snackbar.LENGTH_LONG);
                return;
            }

            //Adicionar ou atualizar usuario
            Usuario usuario = new Usuario(0,
                    et_nome.getText().toString(),
                    this.usuario.getIdade(),
                    this.usuario.getSexo(),
                    this.usuario.getPeso(),
                    this.usuario.getAltura(),
                    et_distancia.getText().toString(),
                    et_tempo.getText().toString());

            realmDAL.atualizarRegistro(Usuario.class, usuario);

            //Ir para a proxima tela
            Intent it = new Intent(getApplicationContext(), MedicaoActivity.class);
            startActivity(it);

        } catch (Exception e) {
            e.printStackTrace();
            mostrarSnackBar("Falha!!!", Snackbar.LENGTH_SHORT);
        }

    };

    private View.OnClickListener onClick_Avancar_MainActivity = view -> {

        try {

            //Validar Tempo
            String[] toSec = et_tempo.getText().toString().split(":");
            if(toSec.length != 3) {
                mostrarSnackBar("O campo tempo deve estar neste formato 00:00:00", Snackbar.LENGTH_LONG);
                return;
            } else if(toSec[0].equals("00") && toSec[1].equals("00")) {
                mostrarSnackBar("Seu teste deve ter uma duração minima de 1 minuto", Snackbar.LENGTH_LONG);
                return;
            } else if(toSec[2].length() == 1) {
                mostrarSnackBar("O campo tempo deve estar neste formato 00:00:00", Snackbar.LENGTH_LONG);
                return;
            }

            //Adicionar ou atualizar usuario
            Usuario usuario = new Usuario(0,
                    et_nome.getText().toString(),
                    usuario_cadastro.getIdade(),
                    usuario_cadastro.getSexo(),
                    usuario_cadastro.getPeso(),
                    usuario_cadastro.getAltura(),
                    et_distancia.getText().toString(),
                    et_tempo.getText().toString());

            realmDAL.atualizarRegistro(Usuario.class, usuario);

            GerenciadorSP.definirSePrimeiroAcesso(getApplicationContext(), false);

            //Ir para a proxima tela
            Intent it = new Intent(getApplicationContext(), MainActivity.class);
            it.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
            startActivity(it);

        } catch (Exception e) {
            e.printStackTrace();
            mostrarSnackBar("Falha!!!", Snackbar.LENGTH_SHORT);
        }

    };

}
