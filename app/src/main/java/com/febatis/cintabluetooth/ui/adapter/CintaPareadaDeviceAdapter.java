package com.febatis.cintabluetooth.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.model.CintaPareadaDeviceItem;

public class CintaPareadaDeviceAdapter extends RecyclerView.Adapter<CintaPareadaDeviceAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onClick(CintaPareadaDeviceItem menuItem);
    }

    private final int VIEW_TYPE_SEM_ITENS = -10;
    private CintaPareadaDeviceItem mData;
    private OnItemClickListener onItemClickListener;

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgStatus;
        TextView tvIdentificador;
        TextView tvEnderecoMac;
        TextView tvStatus;
        Button btnEsquecer;

        TextView tvMsg;

        ViewHolder(View v) {
            super(v);
            imgStatus = v.findViewById(R.id.img_status);
            tvIdentificador = v.findViewById(R.id.tv_identificador);
            tvEnderecoMac = v.findViewById(R.id.tv_endereco_mac);
            tvStatus = v.findViewById(R.id.tv_status);
            btnEsquecer = v.findViewById(R.id.btn_esquecer);

            tvMsg = v.findViewById(R.id.tv_msg);
        }
    }

    public CintaPareadaDeviceAdapter(CintaPareadaDeviceItem myData, OnItemClickListener onItemClickListener) {
        mData = myData;
        this.onItemClickListener = onItemClickListener;

    }

    @NonNull
    @Override
    public CintaPareadaDeviceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;

        if (viewType != VIEW_TYPE_SEM_ITENS)
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cinta_pareada, parent, false);
        else
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sem_bluetooth_device, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (mData != null) {
            holder.tvIdentificador.setText("ID:\n" + mData.getIdentificador());
            holder.tvEnderecoMac.setText("MAC:\n" + mData.getEnderecoMac());
            holder.tvStatus.setText(mData.getStatus() + " BPM");
            holder.btnEsquecer.setOnClickListener(view -> onItemClickListener.onClick(mData));
        } else {
            holder.tvMsg.setText("Não há dispositivos pareados no momento");
        }

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {

        if (mData == null) {
            return VIEW_TYPE_SEM_ITENS;
        }

        return super.getItemViewType(position);
    }

    public void atualizarLista(CintaPareadaDeviceItem mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

}