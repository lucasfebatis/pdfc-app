package com.febatis.cintabluetooth.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.model.CintaDeviceItem;

import java.util.ArrayList;

public class CintaDeviceAdapter extends RecyclerView.Adapter<CintaDeviceAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onClick(CintaDeviceItem menuItem);
    }

    private final int VIEW_TYPE_SEM_ITENS = -10;
    private ArrayList<CintaDeviceItem> mDataset;
    private OnItemClickListener onItemClickListener;

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvIdentificador;
        TextView tvEnderecoMac;

        ViewHolder(View v) {
            super(v);
            tvIdentificador = v.findViewById(R.id.tv_identificador);
            tvEnderecoMac = v.findViewById(R.id.tv_endereco_mac);
        }
    }

    public CintaDeviceAdapter(RecyclerView recyclerView, ArrayList<CintaDeviceItem> myDataset, OnItemClickListener onItemClickListener) {
        mDataset = myDataset;
        this.onItemClickListener = onItemClickListener;

        if (mDataset == null || mDataset.isEmpty()) {
            onCreateViewHolder(recyclerView, VIEW_TYPE_SEM_ITENS);
        }

    }

    @NonNull
    @Override
    public CintaDeviceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;

        if (viewType != VIEW_TYPE_SEM_ITENS)
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cinta_device, parent, false);
        else
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sem_bluetooth_device, parent, false);



        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (mDataset != null && !mDataset.isEmpty()) {
            holder.tvIdentificador.setText("ID: " + mDataset.get(position).getIdentificador());
            holder.tvEnderecoMac.setText("Endereço MAC: " + mDataset.get(position).getEnderecoMac());
        }

        holder.itemView.setOnClickListener(view -> onItemClickListener.onClick(mDataset.get(holder.getAdapterPosition())));

    }

    @Override
    public int getItemCount() {
        return mDataset==null?0:mDataset.size();
    }


    public void atualizarLista(ArrayList<CintaDeviceItem> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

}