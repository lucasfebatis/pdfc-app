package com.febatis.cintabluetooth.ui.activity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.base.BaseActivity;
import com.febatis.cintabluetooth.cinta.CintaClient;
import com.febatis.cintabluetooth.matematica.JamaTestes2;
import com.febatis.cintabluetooth.model.HistoricoItem;
import com.febatis.cintabluetooth.model.Usuario;
import com.febatis.cintabluetooth.realm.RealmDAL;
import com.febatis.cintabluetooth.timer.Timer;
import com.febatis.cintabluetooth.util.MatematicaUtil;
import com.febatis.cintabluetooth.util.MonitorCinta;
import com.febatis.gerenciadordepermisses.GerenciadorPermissoes;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import io.realm.Realm;

import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_ACAO;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_ADD_ITEM;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_ENCONTRADO;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_MSG;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.BLUETOOTH_PAREADO_MSG;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.INTENT_FILTER_BC_BLUETOOTH;
import static com.febatis.cintabluetooth.util.GerenciadorBroadcast.REQUEST_ENABLE_BT;

public class MedicaoActivity extends BaseActivity {

    public static boolean isCicloRapido;

    Timer timer;

    View root;

    String nome;
    String distancia;
    String tempo;

    TextView tv_nome;
    TextView tv_status;
    TextView tv_freq;
    TextView tv_contagem_regressiva;
    Button btn_acao;
    TextView tv_velocidade;
    TextView tv_distancia;
    TextView tv_cronometro;

    ArrayList<Integer> freq;
    ArrayList<Double> velo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicao);

        root = findViewById(R.id.root);
        timer = new Timer(onEvent_Gravar);

        freq = new ArrayList<>();
        velo = new ArrayList<>();

        getIntentValues();
        initViews();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(false);

        if (GerenciadorPermissoes.isPermissionGranted(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            initActivity();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(bc_bluetooth, new IntentFilter(INTENT_FILTER_BC_BLUETOOTH));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(bc_bluetooth);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.log_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_log) {
            Intent it = new Intent(this, LogActivity.class);
            startActivity(it);
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == GerenciadorPermissoes.REQUEST_CODE) {

            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        initActivity();
                    } else {
                        Snackbar.make(root, "Sem permissão", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }

        }

    }

    void initActivity() {
        //itens = new ArrayList<>();

        MonitorCinta.configBluetoothAdapter(this, REQUEST_ENABLE_BT);
        //configRecyclerView(itens);
    }

    public void configBluetoothAdapter() {
        boolean permissao = GerenciadorPermissoes.isBluetoothAdminPermissionGranted(this);

        if (permissao) {
            Snackbar.make(root, "Sim", Snackbar.LENGTH_SHORT).show();

            BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

            if (btAdapter.isEnabled()) {
                CintaClient cintaClient = new CintaClient(this, btAdapter);
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }

        } else {
            Snackbar.make(root, "Não", Snackbar.LENGTH_SHORT).show();
        }
    }

    public void initViews() {

        tv_nome = findViewById(R.id.tv_nome);
        tv_nome.setText(nome);

        tv_status = findViewById(R.id.tv_status);
        tv_freq = findViewById(R.id.tv_freq);

        tv_contagem_regressiva = findViewById(R.id.tv_contagem_regressiva);
        tv_contagem_regressiva.setVisibility(View.INVISIBLE);

        btn_acao = findViewById(R.id.btn_acao);
        btn_acao.setText("Iniciar");
        btn_acao.setEnabled(false);
        btn_acao.setOnClickListener(onClick_Acao);

        tv_distancia = findViewById(R.id.tv_distancia);
        tv_cronometro = findViewById(R.id.tv_cronometro);

        tv_velocidade = findViewById(R.id.tv_velocidade);
        tv_velocidade.setText(MatematicaUtil.velocidadeToString(getVelocidadeInicial()));

    }

    private void getIntentValues() {

        Realm.init(getApplicationContext());
        RealmDAL realmDAL = new RealmDAL(this);
        Usuario usuario = (Usuario) realmDAL.obterRegistro(Usuario.class, 0);

        nome = usuario.getNome();
        distancia = usuario.getDistancia();
        tempo = usuario.getTempo();

    }

    public double getVelocidadeInicial() {
        double distancia = Double.valueOf(this.distancia); //(em km)
        String[] toSecs = this.tempo.split(":");

        double tempo = (Integer.valueOf(toSecs[0]) * 3600) + (Integer.valueOf(toSecs[1]) * 60) + Integer.valueOf(toSecs[2]); //(em sec)
        return MatematicaUtil.arredondar(((distancia * 3600) / tempo) * 0.65);
    }

    //Broadcast
    BroadcastReceiver bc_bluetooth = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String acao = intent.getStringExtra(BLUETOOTH_ACAO);

            switch (acao) {
                case BLUETOOTH_MSG:
                    String msg = intent.getStringExtra(BLUETOOTH_MSG);
                    Snackbar.make(root, msg, Snackbar.LENGTH_SHORT).show();
                    break;
                case BLUETOOTH_ADD_ITEM:
                    String add_item = intent.getStringExtra(BLUETOOTH_ADD_ITEM);

                    if (add_item.contains("Frequência Cardíaca: ")) {
                        tv_status.setText("Status do medidor: Conectado");
                        int vFreq = MatematicaUtil.stringToFrequencia(add_item);
                        tv_freq.setText(MatematicaUtil.frequenciaToString(vFreq));
                        btn_acao.setEnabled(true);
                    } else {
                        tv_status.setText("Status do medidor: Desconectado");
                        btn_acao.setEnabled(false);
                    }

                    //findViewById(R.id.tv_sem_itens).setVisibility(View.GONE);
                    /*
                    itens.add(new BluetoothDeviceItem(add_item, "Descoberto"));
                    mAdapter.atualizarLista(itens);
                    */
                    break;
                case BLUETOOTH_PAREADO_MSG:
                    msg = intent.getStringExtra(BLUETOOTH_MSG);

                    tv_status.setText("Status do medidor: Conectado");
                    tv_freq.setText(msg + " BPM");
                    btn_acao.setEnabled(true);

                    break;
                case BLUETOOTH_ENCONTRADO:
                    //TODO: Implementar quando a cinta for desconectada
                    break;
            }
        }
    };

    //Eventos
    private View.OnClickListener onClick_Acao = view -> {

        if (btn_acao.getText().toString().equals("Iniciar")) {
            timer.startRepeatingTask();
            btn_acao.setText("Parar");
            Drawable btn_parar = getDrawable(R.drawable.btn_parar);
            btn_acao.setBackground(btn_parar);
        } else {
            Intent it = new Intent(getApplicationContext(), ResultadoActivity.class);

            int[] freqPassar = new int[freq.size()];
            double[] veloPassar = new double[velo.size()];

            for (int i = 0; i < freq.size(); i++) {
                freqPassar[i] = freq.get(i);
                veloPassar[i] = velo.get(i);
            }

            it.putExtra("frequencias", freqPassar);
            it.putExtra("velocidades", veloPassar);

            Realm.init(getApplicationContext());
            RealmDAL realmDAL = new RealmDAL(this);

            //Adicionando ao Historico
            double[] freqForPDFC = new double[freqPassar.length];
            for (int i = 0; i < freqForPDFC.length; i++) {
                freqForPDFC[i] = freqPassar[i];
            }

            try {
                realmDAL.addRegistro(HistoricoItem.class,
                        0,
                        nome,
                        veloPassar[veloPassar.length - 1] - veloPassar[0],
                        JamaTestes2.gerarTeste(nome, veloPassar, freqForPDFC).split("pdfc: ")[1],
                        new Date().getTime(),
                        freqForPDFC,
                        veloPassar);
            } catch (Exception e) {
                e.printStackTrace();
            }

            startActivity(it);
        }
    };

    String verificacao = "00:00";
    private Timer.OnCustomEventListener onEvent_Gravar = (tempo) -> {

        if (!tempo.equals(verificacao)) {
            if (!isCicloRapido) {

                //Teste Normal
                if (tempo.equals("01:30")) {
                    //TODO: Grava 1º
                } else if (tempo.equals("02:50")) {
                    //TODO: Grava 2º e sobreescreve a 1º
                    freq.add(MatematicaUtil.stringToFrequencia(tv_freq.getText().toString()));
                    velo.add(MatematicaUtil.stringToVelocicdade(tv_velocidade.getText().toString()));

                    tv_contagem_regressiva.setText("Aumente a velocidade em...");
                } else if (tempo.equals("02:55")) {
                    tv_contagem_regressiva.setVisibility(View.VISIBLE);
                    tv_contagem_regressiva.setText("5");
                } else if (tempo.equals("02:56")) {
                    tv_contagem_regressiva.setText("4");
                } else if (tempo.equals("02:57")) {
                    tv_contagem_regressiva.setText("3");
                } else if (tempo.equals("02:58")) {
                    tv_contagem_regressiva.setText("2");
                } else if (tempo.equals("02:59")) {
                    tv_contagem_regressiva.setText("1");
                } else if (tempo.equals("03:00")) {

                    tv_contagem_regressiva.setVisibility(View.INVISIBLE);
                    double nextVelo = MatematicaUtil.stringToVelocicdade(tv_velocidade.getText().toString()) + 0.5;
                    tv_velocidade.setText(MatematicaUtil.velocidadeToString(nextVelo));

                    tv_distancia.setText("Distância Percorrida: " + (velo.get(velo.size() - 1) - velo.get(0)) + " km");

                    timer.stopRepeatingTask();
                    timer.startRepeatingTask();

                    Snackbar.make(root, "Velocidade aumentada", Snackbar.LENGTH_SHORT).show();
                }
                tv_cronometro.setText("Cronômetro: " + tempo);
                verificacao = tempo;

                /*
                if (tempo.equals("01:30")) {
                    //TODO: Grava 1º
                } else if (tempo.equals("02:50")) {
                    //TODO: Grava 2º e sobreescreve a 1º
                    freq.add(MatematicaUtil.stringToFrequencia(tv_freq.getText().toString()));
                    velo.add(MatematicaUtil.stringToVelocicdade(tv_velocidade.getText().toString()));

                    Snackbar.make(root, "Aumente a velocidade em...", Snackbar.LENGTH_SHORT).show();
                } else if (tempo.equals("02:55")) {
                    tv_cronometro.setVisibility(View.VISIBLE);
                    tv_cronometro.setText("5");
                } else if (tempo.equals("02:56")) {
                    tv_cronometro.setText("4");
                } else if (tempo.equals("02:57")) {
                    tv_cronometro.setText("3");
                } else if (tempo.equals("02:58")) {
                    tv_cronometro.setText("2");
                } else if (tempo.equals("02:59")) {
                    tv_cronometro.setText("1");
                } else if (tempo.equals("03:00")) {

                    tv_cronometro.setVisibility(View.INVISIBLE);
                    double nextVelo = MatematicaUtil.stringToVelocicdade(tv_velocidade.getText().toString()) + 0.5;
                    tv_velocidade.setText(MatematicaUtil.velocidadeToString(nextVelo));

                    timer.stopRepeatingTask();
                    timer.startRepeatingTask();

                    Snackbar.make(root, "Velocidade aumentada", Snackbar.LENGTH_SHORT).show();
                }
                */
            } else {
                //Teste Rápido
                if (tempo.equals("01:30")) {
                    //TODO: Grava 1º
                } else if (tempo.equals("00:04")) {
                    //TODO: Grava 2º e sobreescreve a 1º
                    freq.add(MatematicaUtil.stringToFrequencia(tv_freq.getText().toString()));
                    velo.add(MatematicaUtil.stringToVelocicdade(tv_velocidade.getText().toString()));

                    tv_contagem_regressiva.setText("Aumente a velocidade em...");
                } else if (tempo.equals("00:05")) {
                    tv_contagem_regressiva.setVisibility(View.VISIBLE);
                    tv_contagem_regressiva.setText("5");
                } else if (tempo.equals("00:06")) {
                    tv_contagem_regressiva.setText("4");
                } else if (tempo.equals("00:07")) {
                    tv_contagem_regressiva.setText("3");
                } else if (tempo.equals("00:08")) {
                    tv_contagem_regressiva.setText("2");
                } else if (tempo.equals("00:09")) {
                    tv_contagem_regressiva.setText("1");
                } else if (tempo.equals("00:10")) {

                    tv_contagem_regressiva.setVisibility(View.INVISIBLE);
                    double nextVelo = MatematicaUtil.stringToVelocicdade(tv_velocidade.getText().toString()) + 0.5;
                    tv_velocidade.setText(MatematicaUtil.velocidadeToString(nextVelo));

                    tv_distancia.setText("Distância Percorrida: " + (velo.get(velo.size() - 1) - velo.get(0)) + " km");

                    timer.stopRepeatingTask();
                    timer.startRepeatingTask();

                    Snackbar.make(root, "Velocidade aumentada", Snackbar.LENGTH_SHORT).show();
                }
                tv_cronometro.setText("Cronômetro: " + tempo);
                verificacao = tempo;
            }
        }

    };

}
