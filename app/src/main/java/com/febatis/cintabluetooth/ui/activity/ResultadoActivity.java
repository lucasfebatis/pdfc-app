package com.febatis.cintabluetooth.ui.activity;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.widget.TextView;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.base.BaseActivity;
import com.febatis.cintabluetooth.matematica.JamaTestes2;
import com.febatis.cintabluetooth.model.ResultadoItem;
import com.febatis.cintabluetooth.ui.adapter.ResultadoAdapter;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;

import java.util.ArrayList;

public class ResultadoActivity extends BaseActivity {

    int[] freq;
    double[] velo;

    TextView tv_tempo_total;
    TextView tv_pdfc;

    ResultadoAdapter mAdapter_Freq;
    ResultadoAdapter mAdapter_Velo;

    ArrayList<ResultadoItem> myDataset_Freq;
    ArrayList<ResultadoItem> myDataset_Velo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        initViews();
        getIntentValues();
        configDataSet();
        configRecyclerView();
        obterPDFC();
        initGrafico();

    }

    LineChart mChart;

    private void initGrafico() {

        //X = Velocidade
        //Y = Frequencia

        /*
        TextView tvX = findViewById(R.id.tvXMax);
        TextView tvY = findViewById(R.id.tvYMax);

        mSeekBarX = findViewById(R.id.seekBar1);
        mSeekBarY = findViewById(R.id.seekBar2);

        mSeekBarX.setProgress(45);
        mSeekBarY.setProgress(100);

        mSeekBarY.setOnSeekBarChangeListener(this);
        mSeekBarX.setOnSeekBarChangeListener(this);
        */

        mChart = findViewById(R.id.chart1);
        mChart.setOnChartGestureListener(onChartGestureListener);
        mChart.setOnChartValueSelectedListener(onChartValueSelectedListener);
        mChart.setDrawGridBackground(false);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        // mChart.setScaleXEnabled(true);
        // mChart.setScaleYEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
        // mChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        /*TODO
        MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart
        */

        // x-axis limit line
        LimitLine llXAxis = new LimitLine(10f, "Index 10");
        llXAxis.setLineWidth(4f);
        llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMaximum(Float.valueOf(String.valueOf(velo[freq.length -1])));
        xAxis.setAxisMinimum(Float.valueOf(String.valueOf(velo[0])));
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        //xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());
        //xAxis.addLimitLine(llXAxis); // add x-axis limit line

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.setAxisMaximum(freq[freq.length -1]);
        leftAxis.setAxisMinimum(freq[0] - 10);
        //leftAxis.setYOffset(20f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        mChart.getAxisRight().setEnabled(false);

        //mChart.getViewPortHandler().setMaximumScaleY(2f);
        //mChart.getViewPortHandler().setMaximumScaleX(2f);

        // add data
        setData(freq.length, 100);

//        mChart.setVisibleXRange(20);
//        mChart.setVisibleYRange(20f, AxisDependency.LEFT);
//        mChart.centerViewTo(20, 50, AxisDependency.LEFT);

        //mChart.animateX(2500);
        //mChart.invalidate();

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);

        // // dont forget to refresh the drawing
        // mChart.invalidate();

    }

    private void setData(int count, float range) {

        ArrayList<Entry> values = new ArrayList<>();

        for (int i = 0; i < count; i++) {

            //float val = (float) (Math.random() * range) + 3;
            float valy = freq[i];
            float valx = Float.valueOf(String.valueOf(velo[i]));
            values.add(new Entry(valx, valy, getResources().getDrawable(R.drawable.ic_account_grey600_36dp)));
        }

        LineDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet)mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "");

            set1.setDrawIcons(false);

            // set the line to be drawn like this "- - - - - -"
            set1.enableDashedLine(10f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.BLACK);
            set1.setCircleColor(Color.BLACK);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
                set1.setFillDrawable(drawable);
            }
            else {
                set1.setFillColor(Color.BLACK);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            mChart.setData(data);
        }
    }

    OnChartGestureListener onChartGestureListener = new OnChartGestureListener() {
        @Override
        public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

        }

        @Override
        public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

        }

        @Override
        public void onChartLongPressed(MotionEvent me) {

        }

        @Override
        public void onChartDoubleTapped(MotionEvent me) {

        }

        @Override
        public void onChartSingleTapped(MotionEvent me) {

        }

        @Override
        public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

        }

        @Override
        public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

        }

        @Override
        public void onChartTranslate(MotionEvent me, float dX, float dY) {

        }
    };

    OnChartValueSelectedListener onChartValueSelectedListener = new OnChartValueSelectedListener() {
        @Override
        public void onValueSelected(Entry e, Highlight h) {

        }

        @Override
        public void onNothingSelected() {

        }
    };



    private void obterPDFC() {

        double[] freqForPDFC = new double[freq.length];
        for (int i = 0; i < freqForPDFC.length; i++) {
            freqForPDFC[i] = freq[i];
        }

        String retorno = JamaTestes2.gerarTeste("", velo, freqForPDFC);
        tv_pdfc.setText("Seu PDFC: " + retorno.split("pdfc: ")[1]);
    }

    public void initViews() {
        tv_tempo_total = findViewById(R.id.tv_tempo_total);
        tv_pdfc = findViewById(R.id.tv_pdfc);
    }

    private void configDataSet() {

        myDataset_Freq = new ArrayList<>();
        myDataset_Velo = new ArrayList<>();

        for (int i = 0; i < freq.length; i++) {
            myDataset_Freq.add(new ResultadoItem(String.valueOf(freq[i]) + " bpm"));
            myDataset_Velo.add(new ResultadoItem(String.valueOf(velo[i]) + " km/h"));
        }

    }

    private void configRecyclerView() {

        RecyclerView mRecyclerView_Freq;
        RecyclerView mRecyclerView_Velo;

        RecyclerView.LayoutManager mLayoutManager_Freq;
        RecyclerView.LayoutManager mLayoutManager_Velo;

        mRecyclerView_Freq = findViewById(R.id.listaFreq);
        mRecyclerView_Velo = findViewById(R.id.listaVelo);

        mRecyclerView_Freq.setHasFixedSize(true);
        mRecyclerView_Velo.setHasFixedSize(true);

        mLayoutManager_Freq = new LinearLayoutManager(this);
        mLayoutManager_Velo = new LinearLayoutManager(this);

        /*
        mLayoutManager_Freq = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mLayoutManager_Velo = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        */

        mRecyclerView_Freq.setLayoutManager(mLayoutManager_Freq);
        mRecyclerView_Velo.setLayoutManager(mLayoutManager_Velo);

        mAdapter_Freq = new ResultadoAdapter(mRecyclerView_Freq, myDataset_Freq);
        mAdapter_Velo = new ResultadoAdapter(mRecyclerView_Velo, myDataset_Velo);

        mRecyclerView_Freq.setAdapter(mAdapter_Freq);
        mRecyclerView_Velo.setAdapter(mAdapter_Velo);

    }

    private void getIntentValues() {

        freq = getIntent().getIntArrayExtra("frequencias");
        velo = getIntent().getDoubleArrayExtra("velocidades");

    }



}
