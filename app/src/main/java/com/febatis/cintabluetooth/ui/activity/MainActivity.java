package com.febatis.cintabluetooth.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.base.BaseActivity;
import com.febatis.cintabluetooth.model.MenuItem;
import com.febatis.cintabluetooth.ui.adapter.MenuAdapter;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {

    RecyclerView rv_menu;
    ArrayList<MenuItem> myDataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Testes.gerarTestes();

        obterDados();
        setupRecyclerView(myDataset);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_ajuda) {
            Intent it = new Intent(this, AjudaActivity.class);
            startActivity(it);
            return true;
        }
        return false;
    }

    private void obterDados() {

        myDataset = new ArrayList<>();
        myDataset.add(new MenuItem(R.drawable.ic_book_open_variant_grey600_36dp, "Histórico", ""));
        myDataset.add(new MenuItem(R.drawable.ic_clipboard_text_grey600_36dp, "Novo teste", ""));
        myDataset.add(new MenuItem(R.drawable.ic_account_grey600_36dp, "Meu Perfil", ""));
        myDataset.add(new MenuItem(R.drawable.ic_bluetooth_grey600_36dp, "Conexão", ""));

    }

    private void setupRecyclerView(ArrayList<MenuItem> myDataset) {

        RecyclerView.Adapter mAdapter;
        RecyclerView.LayoutManager mLayoutManager;

        rv_menu = findViewById(R.id.rv_menu);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        rv_menu.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_menu.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MenuAdapter(myDataset, onClick_Menu);
        rv_menu.setAdapter(mAdapter);


    }

    //Eventos
    MenuAdapter.OnItemClickListener onClick_Menu = menuItem -> {

        switch (menuItem.getTitulo()) {
            case "Histórico": {
                Intent it = new Intent(this, HistoricoActivity.class);
                startActivity(it);
                break;
            }
            case "Novo teste": {
                Intent it = new Intent(this, PreTesteActivity.class);
                startActivity(it);
                break;
            }
            case "Meu Perfil": {
                Intent it = new Intent(this, PerfilActivity.class);
                startActivity(it);
                break;
            }
            case "Conexão": {
                Intent it = new Intent(this, ConexaoActivity.class);
                startActivity(it);
                break;
            }
        }
    };
}
