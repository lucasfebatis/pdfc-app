package com.febatis.cintabluetooth.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.base.BaseActivity;
import com.febatis.cintabluetooth.model.Usuario;

import java.util.Objects;

public class CadastroPerfilActivity extends BaseActivity {

    EditText et_nome;
    EditText et_idade;
    EditText et_sexo;
    EditText et_altura;
    EditText et_peso;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_perfil);

        initViews();
        configActivity();
    }

    private void configActivity() {

        boolean isIntro = getIntent().getBooleanExtra("isIntro", false);

        if (isIntro) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(false);

            setFloatButtonListener(onClick_FloatButton);
        }


    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void initViews() {

        et_nome = findViewById(R.id.et_nome);
        et_idade = findViewById(R.id.et_idade);
        et_sexo = findViewById(R.id.et_sexo);
        et_altura = findViewById(R.id.et_altura);
        et_peso = findViewById(R.id.et_peso);

    }

    //eventos
    View.OnClickListener onClick_FloatButton = view -> {

        try {
            //Adicionar ou atualizar usuario
            Usuario usuario = new Usuario(0,
                    et_nome.getText().toString(),
                    Integer.valueOf(et_idade.getText().toString()),
                    et_sexo.getText().toString(),
                    Double.valueOf(et_peso.getText().toString()),
                    Double.valueOf(et_altura.getText().toString()),
                    null,
                    null);

            Intent intent = new Intent(getApplicationContext(), PreTesteActivity.class);
            intent.putExtra("isIntro", true);
            intent.putExtra("usuario", usuario);
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }

    };
}