package com.febatis.cintabluetooth.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.base.BaseActivity;
import com.febatis.cintabluetooth.base.Temas;
import com.febatis.cintabluetooth.model.Usuario;
import com.febatis.cintabluetooth.realm.RealmDAL;
import com.febatis.cintabluetooth.util.GerenciadorSP;
import com.febatis.cintabluetooth.util.MaskEditUtil;

import io.realm.Realm;

public class PerfilActivity extends BaseActivity {

    Spinner spinner;
    Button btn_editar_pessoais;
    Button btn_editar_pre_teste;
    Button btn_alterar;

    EditText et_nome;
    EditText et_idade;
    EditText et_sexo;
    EditText et_altura;
    EditText et_peso;


    EditText et_distancia;
    EditText et_tempo;

    RealmDAL realmDAL;

    String[] arraySpinner = new String[]{
            Temas.PADRAO_ANDROID_EXIBICAO, Temas.PDFC_CLARO_EXIBICAO, Temas.PDFC_ESCURO_EXIBICAO
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        initViews();
        initUsuario();
        String temaId = GerenciadorSP.obterTema(getApplicationContext());
        selecionarItemSpinner(obterStringPeloTema(temaId));
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void initViews() {

        setupSpinner();

        //Card Pessoais
        et_nome = findViewById(R.id.et_nome);
        et_idade = findViewById(R.id.et_idade);
        et_sexo = findViewById(R.id.et_sexo);
        et_altura = findViewById(R.id.et_altura);
        et_peso = findViewById(R.id.et_peso);


        //Card Pré Teste
        et_distancia = findViewById(R.id.et_distancia);

        et_tempo = findViewById(R.id.et_tempo);
        et_tempo.addTextChangedListener(MaskEditUtil.mask(et_tempo, "##:##:##"));

        btn_editar_pre_teste = findViewById(R.id.btn_editar_pre_teste);
        btn_editar_pre_teste.setOnClickListener(btn_editar_pre_teste_onClick);


        //Card Tema
        btn_alterar = findViewById(R.id.btn_alterar);
        btn_alterar.setOnClickListener(btn_alterar_onClick);

    }

    private void initUsuario() {

        Realm.init(getApplicationContext());
        realmDAL = new RealmDAL(this);

        try {
            Usuario usuario = (Usuario) realmDAL.obterRegistro(Usuario.class, 0);
            et_nome.setText(usuario.getNome());

            et_idade.setText(String.valueOf(usuario.getIdade()));
            et_sexo.setText(usuario.getSexo());
            et_altura.setText(String.valueOf(usuario.getAltura()));
            et_peso.setText(String.valueOf(usuario.getPeso()));

            et_distancia.setText(usuario.getDistancia());
            et_tempo.setText(usuario.getTempo());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    void setupSpinner() {
        spinner = findViewById(R.id.spinner_tema);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, arraySpinner);
        spinner.setAdapter(adapter);
    }

    void selecionarItemSpinner(String itemSelecionado) {

        for (int i = 0; i < arraySpinner.length; i++) {
            if (arraySpinner[i].equals(itemSelecionado)) {
                spinner.setSelection(i);
            }

        }

    }

    private String obterTemaPelaString(String s) {

        switch (s) {
            case Temas.PADRAO_ANDROID_EXIBICAO:
                return Temas.PADRAO_ANDROID;
            case Temas.PDFC_CLARO_EXIBICAO:
                return Temas.PDFC_CLARO;
            case Temas.PDFC_ESCURO_EXIBICAO:
                return Temas.PDFC_ESCURO;
            default:
                return Temas.PDFC_CLARO;
        }
    }

    private String obterStringPeloTema(String s) {

        switch (s) {
            case Temas.PADRAO_ANDROID:
                return Temas.PADRAO_ANDROID_EXIBICAO;
            case Temas.PDFC_CLARO:
                return Temas.PDFC_CLARO_EXIBICAO;
            case Temas.PDFC_ESCURO:
                return Temas.PDFC_ESCURO_EXIBICAO;
            default:
                return Temas.PDFC_CLARO_EXIBICAO;
        }
    }

    //eventos

    View.OnClickListener btn_editar_pre_teste_onClick = view -> {

        try {

            //Validar Tempo
            String[] toSec = et_tempo.getText().toString().split(":");
            if(toSec.length != 3) {
                mostrarSnackBar("O campo tempo deve estar neste formato 00:00:00", Snackbar.LENGTH_LONG);
                return;
            } else if(toSec[0].equals("00") && toSec[1].equals("00")) {
                mostrarSnackBar("Seu teste deve ter uma duração minima de 1 minuto", Snackbar.LENGTH_LONG);
                return;
            } else if(toSec[2].length() == 1) {
                mostrarSnackBar("O campo tempo deve estar neste formato 00:00:00", Snackbar.LENGTH_LONG);
                return;
            }


            //Adicionar ou atualizar usuario
            Usuario usuario = new Usuario(0,
                    et_nome.getText().toString(),
                    Integer.valueOf(et_idade.getText().toString()),
                    et_sexo.getText().toString(),
                    Double.valueOf(et_peso.getText().toString()),
                    Double.valueOf(et_altura.getText().toString()),
                    et_distancia.getText().toString(),
                    et_tempo.getText().toString());

            realmDAL.atualizarRegistro(Usuario.class, usuario);

            mostrarSnackBar("Dados pessoais alterados com sucesso!!!", Snackbar.LENGTH_SHORT);

        } catch (Exception e) {
            e.printStackTrace();
            mostrarSnackBar("Falha!!!", Snackbar.LENGTH_SHORT);
        }

    };

    View.OnClickListener btn_alterar_onClick = view -> {

        alterarTema(obterTemaPelaString(spinner.getSelectedItem().toString()));

        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);

    };
}