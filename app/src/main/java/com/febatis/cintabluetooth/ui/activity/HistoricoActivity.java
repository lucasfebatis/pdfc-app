package com.febatis.cintabluetooth.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.base.BaseActivity;
import com.febatis.cintabluetooth.model.HistoricoItem;
import com.febatis.cintabluetooth.realm.RealmDAL;
import com.febatis.cintabluetooth.ui.adapter.HistoricoAdapter;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class HistoricoActivity extends BaseActivity {

    RecyclerView rv_historico;
    ArrayList<HistoricoItem> myDataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico);

        obterDados();
        setupRecyclerView(myDataset);
    }

    private void obterDados() {

        /*
        myDataset = new ArrayList<>();
        myDataset.add(new HistoricoItem(Testes.Mathias.class.getSimpleName(),"0km","0", new Date().getTime(), Testes.Mathias.velocidades, Testes.Mathias.frequencias));
        myDataset.add(new HistoricoItem("Arthur","0.5km","0.5", new Date().getTime(), Testes.Gustavo.velocidades, Testes.Gustavo.frequencias));
        myDataset.add(new HistoricoItem("Monica","1km","1", new Date().getTime(), Testes.Felipe.velocidades, Testes.Felipe.frequencias));
        myDataset.add(new HistoricoItem("Telma","10.5km","5", new Date().getTime(), Testes.Batista.velocidades, Testes.Batista.frequencias));
        myDataset.add(new HistoricoItem("Jose","90.0km","70", new Date().getTime(), Testes.Diogo.velocidades, Testes.Diogo.frequencias));
        myDataset.add(new HistoricoItem("Batista","12.5km","9", new Date().getTime(), Testes.Richie.velocidades, Testes.Richie.frequencias));
        myDataset.add(new HistoricoItem("Amanda","24.5km","20", new Date().getTime(), Testes.Larissa.velocidades, Testes.Larissa.frequencias));
        myDataset.add(new HistoricoItem("Julia","23.5km","17", new Date().getTime(), Testes.Arthur.velocidades, Testes.Arthur.frequencias));
        myDataset.add(new HistoricoItem("Andrea","34.0km","30", new Date().getTime(), Testes.Rafael.velocidades, Testes.Rafael.frequencias));
        myDataset.add(new HistoricoItem("Andre","44km","34", new Date().getTime(), Testes.Rodrigo.velocidades, Testes.Rodrigo.frequencias));
        myDataset.add(new HistoricoItem("Gustavo","30km","21", new Date().getTime(), Testes.Cesar.velocidades, Testes.Cesar.frequencias));
        myDataset.add(new HistoricoItem("Ari","50km","10", new Date().getTime(), Testes.Mauricio.velocidades, Testes.Mauricio.frequencias));
        */

        Realm.init(getApplicationContext());
        RealmDAL realmDAL = new RealmDAL(this);

        try {
            RealmResults<HistoricoItem> historicoItens = (RealmResults<HistoricoItem>) realmDAL.obterTodosRegistros(HistoricoItem.class);
            myDataset = new ArrayList<>();
            myDataset.addAll(historicoItens);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(myDataset == null || myDataset.isEmpty()) {
            findViewById(R.id.tv_msg).setVisibility(View.VISIBLE);
            findViewById(R.id.rv_historico).setVisibility(View.GONE);
        }

    }

    private void setupRecyclerView(ArrayList<HistoricoItem> myDataset) {

        RecyclerView.Adapter mAdapter;
        RecyclerView.LayoutManager mLayoutManager;

        rv_historico = findViewById(R.id.rv_historico);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        rv_historico.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        rv_historico.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new HistoricoAdapter(myDataset, onClick_Historico);
        rv_historico.setAdapter(mAdapter);

    }

    //Eventos
    HistoricoAdapter.OnItemClickListener onClick_Historico = historicoItem -> {

        Intent it = new Intent(getApplicationContext(), ResultadoActivity.class);

        int[] freqPassar = new int[historicoItem.getFreq().length];

        for (int i = 0; i < historicoItem.getFreq().length; i++) {
            freqPassar[i] = Double.valueOf(historicoItem.getFreq()[i]).intValue();
        }

        it.putExtra("frequencias", freqPassar);
        it.putExtra("velocidades", historicoItem.getVelo());
        startActivity(it);

    };
}
