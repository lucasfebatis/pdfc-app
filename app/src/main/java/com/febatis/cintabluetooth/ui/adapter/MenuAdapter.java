package com.febatis.cintabluetooth.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.model.MenuItem;

import java.util.ArrayList;

import me.biubiubiu.justifytext.library.JustifyTextView;

/**
 * Criado por lfeba em 20/05/2018.
 */
public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onClick(MenuItem menuItem);
    }

    private ArrayList<MenuItem> mDataset;
    private OnItemClickListener onItemClickListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView tv_titulo;
        JustifyTextView tv_descricao;
        ViewHolder(View v) {
            super(v);
            tv_titulo = v.findViewById(R.id.tv_titulo);
            tv_descricao = v.findViewById(R.id.tv_descricao);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MenuAdapter(ArrayList<MenuItem> myDataset, OnItemClickListener onItemClickListener) {
        mDataset = myDataset;
        this.onItemClickListener = onItemClickListener;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_menu, parent, false);
        //...
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tv_titulo.setText("\t" + mDataset.get(position).getTitulo());
        holder.tv_titulo.setCompoundDrawablesWithIntrinsicBounds(mDataset.get(position).getIdResource(), 0, 0, 0);
        holder.tv_descricao.setText("\t" + mDataset.get(position).getDescricao());

        if(mDataset.get(position).getDescricao().equals("")) {
            holder.tv_descricao.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(view -> onItemClickListener.onClick(mDataset.get(holder.getAdapterPosition())));

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}