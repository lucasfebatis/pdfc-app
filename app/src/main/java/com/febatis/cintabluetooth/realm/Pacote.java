package com.febatis.cintabluetooth.realm;

/**
 * Criado por lfeba em 22/04/2018.
 */

public class Pacote {

    private Object[] objects;

    public Pacote(Object[] objects) {
        this.objects = objects;
    }

    public Object[] getObjects() {
        return objects;
    }

    public void setObjects(Object[] objects) {
        this.objects = objects;
    }
}
