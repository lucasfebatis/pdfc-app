package com.febatis.cintabluetooth.realm;

import android.app.Activity;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

@SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
public class RealmDAL {

    private Realm realm;

    public Realm getRealm() {
        return realm;
    }

    public RealmDAL(Activity activity) {

        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("realm_bd.realm")
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);

        realm = Realm.getDefaultInstance();
    }

    //Criar, Ler, Atualizar, Remover
    public RealmResults<?> obterTodosRegistros(Class classe) {
        return realm.where(classe).findAll();
    }

    @SuppressWarnings("unused")
    public Object obterRegistro(Class classe, int id) {
        return classe.cast(obterTodosRegistros(classe).where().equalTo("id", id).findAll().get(0));
    }

    @SuppressWarnings("ConstantConditions")
    public Object addRegistro(Class<?> classe, Object... atributos) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        //Obter último ID
        //Popular novo objeto
        //Registrar no Realm

        Object obj = null;
        Field[] fields = classe.getDeclaredFields();
        long id = 0;

        //Obtendo último id
        if (obterTodosRegistros(classe).size() != 0) {
            obj = obterTodosRegistros(classe).sort("id", Sort.DESCENDING).get(0);
            RealmResults<?> resultArray = obterTodosRegistros(classe).sort("id", Sort.DESCENDING);


            long ultimoId = 0;

            if (resultArray != null && !resultArray.isEmpty()) {
                JSONObject trendList = new JSONObject(resultArray.get(0).toString());
                ultimoId = trendList.getLong("id");
            }

            id = ultimoId + 1;
        }

        //Setando valores
        Constructor contr = classe.getDeclaredConstructor(Pacote.class);
        Object tex = contr.newInstance(new Pacote(atributos.clone()));

        obj = classe.cast(tex);

        for (Field field : fields) {
            if (field.getName().contains("id")) {
                field.setLong(obj, id);
                break;
            }
        }

        //Registrando no Realm
        realm.beginTransaction();

        if (obj instanceof RealmObject)
            realm.copyToRealm((RealmObject) obj);

        realm.commitTransaction();


        return obj;
    }


    public Object atualizarRegistro(Class<?> classe, Object objeto) {

        if (objeto instanceof RealmObject) {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate((RealmObject) objeto);
            realm.commitTransaction();
        } else {
            objeto = null;
        }

        return objeto;
    }

    public Object removerRegistro(Class<?> classe, int id) {

        Object objeto = obterRegistro(classe, id);

        if (objeto instanceof RealmObject) {
            realm.beginTransaction();
            ((RealmObject) objeto).deleteFromRealm();
            realm.commitTransaction();
        }

        return objeto;
    }
}
