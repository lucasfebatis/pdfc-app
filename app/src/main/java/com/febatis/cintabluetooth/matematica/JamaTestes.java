package com.febatis.cintabluetooth.matematica;

import android.util.Log;

import com.febatis.cintabluetooth.util.PolynomialRootFinder;

import org.ejml.data.Complex64F;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import Jama.LUDecomposition;
import Jama.Matrix;

/**
 * Criado por lfeba em 14/04/2018.
 */

@Deprecated
public class JamaTestes {

    private final static String TAG = "JamaTestes";

    @SuppressWarnings("unused")
    public static void exemploMultiplicacao() {

        Log.e(TAG, "\n Exemplo de Multiplicação");

        //Vetor (1 x 3)
        double[] array1d = {1., 1., 1.};

        //Matriz 2D (3 x 3)
        double[][] array2d = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};

        //Cria os objetos do JAMA
        Matrix a = new Matrix(array2d);
        Matrix b = new Matrix(array1d, 1);

        //Multipica B * A
        Matrix c = b.times(a);

        //Escreve ressultado no Log
        Log.e(TAG, Arrays.deepToString(array2d));
        Log.e(TAG, Arrays.deepToString(new Double[]{1., 1., 1.}));
        Log.e(TAG, Arrays.deepToString(c.getArray()));

    }

    @SuppressWarnings("unused")
    public static void exemploObterResidual() {

        Log.e(TAG, "\n Exemplo de obter Residual");

        double[][] values = {{1, 1, 2}, {2, 4, -3}, {3, 6, -5}};  // each array is a row in the matrix
        double[] rhs = {9, 1, 0}; // rhs vector
        double[] answer = {1, 2, 3}; // this is the answer that you should get.

        Matrix a = new Matrix(values);
        a.print(10, 2);
        LUDecomposition luDecomposition = new LUDecomposition(a);
        luDecomposition.getL().print(10, 2); // lower matrix
        luDecomposition.getU().print(10, 2); // upper matrix

        Matrix b = new Matrix(rhs, rhs.length);
        Matrix x = luDecomposition.solve(b); // solve Ax = b for the unknown vector x
        x.print(10, 2); // print the solution
        Matrix residual = a.times(x).minus(b); // calculate the residual error
        double rnorm = residual.normInf(); // get the max error (yes, it's very small)

        System.out.println("residual: " + rnorm);
        Log.e(TAG, "residual: " + rnorm);

    }

    @SuppressWarnings("unused")
    public static void exemploInverterMatriz() {

        Log.e(TAG, "\n Exemplo de Inverter Matriz");

        //Vetor (1 x 3)
        double[] array1d = {1., 1., 1.};

        //Matriz 2D (3 x 3)
        double[][] array2d = {{1, 2, 1}, {3, 1, 4}, {2, 4, 5}};

        //Matriz 2D (10 x 10)
        double[][] array2d_2 = {{1, 2, 1, 0}, {3, 1, 4, 0}, {2, 4, 5, 0}, {2, 4, 5, 0}};

        //Cria os objetos do JAMA
        Matrix a = new Matrix(array2d_2);
        Matrix b = new Matrix(array1d, 1);

        //Inverte A
        Matrix c = a.inverse();

        //Multiplica B * A
        Matrix d = b.times(a);

        //Escreve ressultado no Log
        Log.e(TAG, Arrays.deepToString(array2d));
        Log.e(TAG, Arrays.deepToString(c.getArray()));

    }

    public static String gerarTeste(String individuo, double[] velocidades, double[] frequencias) {

        Log.e(TAG, "\n Exemplo de Fluxo");

        //Com as velocidade e frequencias obtidas

        //Criamos uma matriz limpa com tamanho da qtd de leituras
        double[][] matrizVelocidades = new double[velocidades.length][velocidades.length];

        //Realizamos os calculos para obter a matriz de velocidades
        //Coluna
        for (int i = 0; i < matrizVelocidades.length; i++) {

            //Linha
            for (int j = 0; j < matrizVelocidades.length; j++) {

                matrizVelocidades[j][i] = Math.pow(velocidades[i], matrizVelocidades.length - (j + 1));

            }

        }

        //Invertemos e multiplicamos pelo vetor de frequencia para obter os coeficientes
        Matrix matrizVelocidadesInvertida = new Matrix(matrizVelocidades).inverse();

        Matrix a = new Matrix(frequencias, 1);
        Matrix matrizCoeficientes = a.times(matrizVelocidadesInvertida);

        Log.e(TAG, "Matriz: " + Arrays.deepToString(matrizVelocidades));
        Log.e(TAG, "Matriz Invertida: " + Arrays.deepToString(matrizVelocidadesInvertida.getArray()));
        Log.e(TAG, "Matriz Coeficientes: " + Arrays.deepToString(matrizCoeficientes.getArray()));

        //Achando o coeficiente angular da reta tangente
        double delta_f = frequencias[frequencias.length - 1] - frequencias[0];
        double delta_v = velocidades[velocidades.length - 1] - velocidades[0];

        double coeficienteAngular = delta_f / delta_v;


        //Construindo Equação
        // (a*x^9) + (b*x^8) + (c*x^7) + (...) + (i*x+j)
        // (resultados[i]*x^(resultados.length-1)) + (resultados[i-1]*x^(resultados.length-2)) + (...)
        Log.e("Equação: ", escreverEquacao(velocidades, frequencias, matrizCoeficientes, coeficienteAngular));

        double[] coeficientesInvertido = matrizCoeficientes.getArray()[0].clone();
        Collections.reverse(Arrays.asList(coeficientesInvertido));

        double[] invertido = new double[matrizCoeficientes.getArray()[0].length];

        for (int i = 0; i < invertido.length / 2; i++) {
            double temp = matrizCoeficientes.getArray()[0][i];
            invertido[i] = matrizCoeficientes.getArray()[0][invertido.length - i - 1];
            invertido[invertido.length - i - 1] = temp;
        }


        //Obtendo derivadas
        double[] funcao = matrizCoeficientes.getArray()[0];
        double[] dFuncao = new double[matrizCoeficientes.getArray()[0].length - 1];

        for (int i = (dFuncao.length - 1); i >= 0; i--) {
            dFuncao[i] = funcao[i] * (funcao.length - (i + 1));
        }

        dFuncao[dFuncao.length - 1] = dFuncao[dFuncao.length - 1] - coeficienteAngular;

        double[] dFuncaoInvertido = dFuncao.clone();

        for (int i = 0; i < dFuncaoInvertido.length / 2; i++) {
            double temp = dFuncao[i];
            dFuncaoInvertido[i] = dFuncao[dFuncaoInvertido.length - i - 1];
            dFuncaoInvertido[dFuncaoInvertido.length - i - 1] = temp;
        }

        Complex64F[] raizes = PolynomialRootFinder.findRoots(dFuncaoInvertido);

        /*Filtrando
        Não pode ser maior que max velocidade
        Não pode ser menor que o min velocidade
        Não pode ser complexo
        */
        ArrayList<Double> filtragem = new ArrayList<>();
        for (Complex64F valor : raizes) {
            if(valor.isReal() &&
                    valor.getReal() > velocidades[0] &&
                    valor.getReal() < velocidades[velocidades.length - 1]) {
                filtragem.add(valor.getReal());
            }
        }
        double[] x_rt = new double[filtragem.size()];
        for (int i = 0; i < filtragem.size(); i++) {
            x_rt[i] = filtragem.get(i);
        }

        //x_rt = pontos x na reta tangente
        //y_rt = pontos y na reta tangente
        double[] y_rt = new double[filtragem.size()];

        for (int i = 0; i < filtragem.size(); i++) {
            y_rt[i] = frequencias[0] + coeficienteAngular * (x_rt[i] - velocidades[0]);
        }


        //Coeficiente da reta perpendicular
        double c_rp = Math.pow(-(coeficienteAngular),(-1));

        //x_rp = pontos x na reta perpendicular
        double[] x_rp = new double[filtragem.size()];

        for(int i = 0; i < y_rt.length; i++) {
            double[] resultante = funcao.clone();
            resultante[4] = funcao[4] - c_rp;
            resultante[5] = funcao[5] - (c_rp * x_rt[i] + y_rt[i]);

            double[] resultanteInvertido = new double[resultante.length];
            for (int j = 0; j < resultanteInvertido.length / 2; j++) {
                double temp = resultante[j];
                resultanteInvertido[j] = resultante[resultanteInvertido.length - j - 1];
                resultanteInvertido[resultanteInvertido.length - j - 1] = temp;
            }

            //Complex64F[] raizesResultante = PolynomialRootFinder.findRoots(resultanteInvertido);
            Complex64F[] raizesResultante = PolynomialRootFinder.findRoots(velocidades);
            ArrayList<Double> filtragemResultante = new ArrayList<>();
            for (Complex64F valor : raizesResultante) {
                if(valor.isReal() &&
                        valor.getReal() < x_rt[i]) {
                    filtragemResultante.add(valor.getReal());
                }
            }

            double maior;
            try {
                maior = Collections.max(filtragemResultante);
            } catch (Exception ex) {
                Log.d(TAG, "Resultado: Não há raiz real de resultante");
                return "Individuo: " + individuo + "\n" +
                        "Resultado: Não há raiz real de resultante";
            }
            x_rp[i] = maior;
        }

        //y_rp = pontos y na reta perpendicular
        double[] y_rp = new double[filtragem.size()];
        for (int i = 0; i < filtragem.size(); i++) {
            y_rp[i] = y_rt[i] + c_rp * (x_rp[i] - x_rt[i]);
        }

        //Obtendo Distancia
        double[] distancia = new double[filtragem.size()];
        for (int i = 0; i < filtragem.size(); i++) {
            double y_dif = Math.pow((y_rp[i] - y_rt[i]), 2);
            double x_dif = Math.pow((x_rp[i] - x_rt[i]), 2);
            distancia[i] = Math.sqrt(y_dif + x_dif);
        }
        ArrayList<Double> distanciaLista = new ArrayList<>();
        for(double d : distancia) {
            distanciaLista.add(d);
        }

        //Obtendo PDFC (Ponto de Deflexão da Frequência Cardíaca)
        double maior = Collections.max(distanciaLista);
        double pdfc = filtragem.get(distanciaLista.indexOf(maior));

        Log.e(TAG, "Resultado: Indivíduo: " + individuo);
        Log.e(TAG, "Resultado: pdfc: " + pdfc);

        return "Indivíduo: " + individuo + "\n" +
                "pdfc: " + pdfc;

    }

    private static String escreverEquacao(double[] velocidades, double[] frequencias, Matrix matrizCoeficientes, double coeficienteAngular) {

        StringBuilder equacao = new StringBuilder();
        equacao.append("(");

        for (int i = 0; i < matrizCoeficientes.getArray().length; i++) {

            double[] resultados = matrizCoeficientes.getArray()[i];

            for (int j = 0; j < matrizCoeficientes.getArray()[0].length; j++) {

                if (j != matrizCoeficientes.getArray()[0].length)
                    equacao.append("(").append(resultados[j]).append("*x^").append(resultados.length - (j + 1)).append(") + ");
                else
                    equacao.append("(").append(resultados[j]).append("*x^").append(resultados.length - (j + 1)).append(")");

                //resultados[i]*x^(resultados.length-1)+resultados[i-1]*x^(resultados.length-2)+
            }
        }

        equacao.append(")");

        //matrizCoeficientes[primeiro valor(acho que é 143)]+coeficienteAngular*(x-matrizVelocidades[primeiro valor (12,5)])
        equacao.append(" -  ((").append(frequencias[0]).append(" + ").append(coeficienteAngular).append(") * (x - ").append(velocidades[0]).append("))");

        return equacao.toString();	
    }
}

