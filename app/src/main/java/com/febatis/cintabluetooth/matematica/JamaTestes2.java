package com.febatis.cintabluetooth.matematica;

import android.os.Build;
import android.util.Log;

import com.febatis.cintabluetooth.util.PolynomialRootFinder;

import org.ejml.data.Complex64F;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.DoubleStream;

import Jama.LUDecomposition;
import Jama.Matrix;

/**
 * Criado por lfeba em 14/04/2018.
 */

public class JamaTestes2 {

    private final static String TAG = "JamaTestes";

    @SuppressWarnings("unused")
    public static void exemploMultiplicacao() {

        Log.e(TAG, "\n Exemplo de Multiplicação");

        //Vetor (1 x 3)
        double[] array1d = {1., 1., 1.};

        //Matriz 2D (3 x 3)
        double[][] array2d = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};

        //Cria os objetos do JAMA
        Matrix a = new Matrix(array2d);
        Matrix b = new Matrix(array1d, 1);

        //Multipica B * A
        Matrix c = b.times(a);

        //Escreve ressultado no Log
        Log.e(TAG, Arrays.deepToString(array2d));
        Log.e(TAG, Arrays.deepToString(new Double[]{1., 1., 1.}));
        Log.e(TAG, Arrays.deepToString(c.getArray()));

    }

    @SuppressWarnings("unused")
    public static void exemploObterResidual() {

        Log.e(TAG, "\n Exemplo de obter Residual");

        double[][] values = {{1, 1, 2}, {2, 4, -3}, {3, 6, -5}};  // each array is a row in the matrix
        double[] rhs = {9, 1, 0}; // rhs vector
        double[] answer = {1, 2, 3}; // this is the answer that you should get.

        Matrix a = new Matrix(values);
        a.print(10, 2);
        LUDecomposition luDecomposition = new LUDecomposition(a);
        luDecomposition.getL().print(10, 2); // lower matrix
        luDecomposition.getU().print(10, 2); // upper matrix

        Matrix b = new Matrix(rhs, rhs.length);
        Matrix x = luDecomposition.solve(b); // solve Ax = b for the unknown vector x
        x.print(10, 2); // print the solution
        Matrix residual = a.times(x).minus(b); // calculate the residual error
        double rnorm = residual.normInf(); // get the max error (yes, it's very small)

        System.out.println("residual: " + rnorm);
        Log.e(TAG, "residual: " + rnorm);

    }

    @SuppressWarnings("unused")
    public static void exemploInverterMatriz() {

        Log.e(TAG, "\n Exemplo de Inverter Matriz");

        //Vetor (1 x 3)
        double[] array1d = {1., 1., 1.};

        //Matriz 2D (3 x 3)
        double[][] array2d = {{1, 2, 1}, {3, 1, 4}, {2, 4, 5}};

        //Matriz 2D (10 x 10)
        double[][] array2d_2 = {{1, 2, 1, 0}, {3, 1, 4, 0}, {2, 4, 5, 0}, {2, 4, 5, 0}};

        //Cria os objetos do JAMA
        Matrix a = new Matrix(array2d_2);
        Matrix b = new Matrix(array1d, 1);

        //Inverte A
        Matrix c = a.inverse();

        //Multiplica B * A
        Matrix d = b.times(a);

        //Escreve ressultado no Log
        Log.e(TAG, Arrays.deepToString(array2d));
        Log.e(TAG, Arrays.deepToString(c.getArray()));

    }

    public static String gerarTeste(String individuo, double[] velocidades, double[] frequencias) {

        Log.e(TAG, "\n Exemplo de Fluxo");

        //Com as velocidade e frequencias obtidas

        //Achando o coeficiente angular da reta tangente
        double delta_f = frequencias[frequencias.length - 1] - frequencias[0];
        double delta_v = velocidades[velocidades.length - 1] - velocidades[0];

        //Coeficiente da reta tangente
        double c_rt = delta_f / delta_v;

        //Coeficiente da reta perpendicular
        double c_rp = Math.pow(-(c_rt), (-1));

        //Criamos uma matriz limpa com tamanho da qtd de leituras
        double[][] matrizVelocidades = new double[velocidades.length][velocidades.length];

        //Realizamos os calculos para obter a matriz de velocidades
        //Coluna
        for (int i = 0; i < matrizVelocidades.length; i++) {

            //Linha
            for (int j = 0; j < matrizVelocidades.length; j++) {

                matrizVelocidades[j][i] = Math.pow(velocidades[i], matrizVelocidades.length - (j + 1));

            }

        }

        //Invertemos e multiplicamos pelo vetor de frequencia para obter os coeficientes
        Matrix matrizVelocidadesInvertida = new Matrix(matrizVelocidades).inverse();

        Matrix a = new Matrix(frequencias, 1);
        Matrix matrizCoeficientes = a.times(matrizVelocidadesInvertida);

        Log.e(TAG, "Matriz: " + Arrays.deepToString(matrizVelocidades));
        Log.e(TAG, "Matriz Invertida: " + Arrays.deepToString(matrizVelocidadesInvertida.getArray()));
        Log.e(TAG, "Matriz Coeficientes: " + Arrays.deepToString(matrizCoeficientes.getArray()));


        //Construindo Equação
        // (a*x^9) + (b*x^8) + (c*x^7) + (...) + (i*x+j)
        // (resultados[i]*x^(resultados.length-1)) + (resultados[i-1]*x^(resultados.length-2)) + (...)
        Log.e("Equação: ", escreverEquacao(velocidades, frequencias, matrizCoeficientes, c_rt));

        double[] coeficientesInvertido = matrizCoeficientes.getArray()[0].clone();
        Collections.reverse(Arrays.asList(coeficientesInvertido));

        double[] matrizCoeficientesInvertida = new double[matrizCoeficientes.getArray()[0].length];

        for (int i = 0; i < matrizCoeficientesInvertida.length / 2; i++) {
            double temp = matrizCoeficientes.getArray()[0][i];
            matrizCoeficientesInvertida[i] = matrizCoeficientes.getArray()[0][matrizCoeficientesInvertida.length - i - 1];
            matrizCoeficientesInvertida[matrizCoeficientesInvertida.length - i - 1] = temp;
        }


        //Obtendo derivadas
        double[] funcao = matrizCoeficientes.getArray()[0];
        double[] derivadaMatrizCoeficientes = new double[matrizCoeficientes.getArray()[0].length - 1];

        for (int i = (derivadaMatrizCoeficientes.length - 1); i >= 0; i--) {
            derivadaMatrizCoeficientes[i] = funcao[i] * (funcao.length - (i + 1));
        }

        derivadaMatrizCoeficientes[derivadaMatrizCoeficientes.length - 1] = derivadaMatrizCoeficientes[derivadaMatrizCoeficientes.length - 1] - c_rt;

        double[] dFuncaoInvertido = derivadaMatrizCoeficientes.clone();

        for (int i = 0; i < dFuncaoInvertido.length / 2; i++) {
            double temp = derivadaMatrizCoeficientes[i];
            dFuncaoInvertido[i] = derivadaMatrizCoeficientes[dFuncaoInvertido.length - i - 1];
            dFuncaoInvertido[dFuncaoInvertido.length - i - 1] = temp;
        }

        Complex64F[] raizes = PolynomialRootFinder.findRoots(dFuncaoInvertido);

        /*Filtrando
        Não pode ser maior que max velocidade
        Não pode ser menor que o min velocidade
        Não pode ser complexo
        */
        ArrayList<Double> filtragem = new ArrayList<>();
        for (Complex64F valor : raizes) {
            if (valor.isReal() &&
                    valor.getReal() > velocidades[0] &&
                    valor.getReal() < velocidades[velocidades.length - 1]) {
                filtragem.add(valor.getReal());
            }
        }

        //x_rp = pontos x em que a reta tangencia a função e onde a reta perpendicular cruza a função
        double[] x_rp = new double[filtragem.size()];
        for (int i = 0; i < filtragem.size(); i++) {
            x_rp[i] = filtragem.get(i);
        }

        //Substituindo x_rp na função para achar y_rp
        //y_rp = pontos y em que a reta tangencia a função e onde a reta perpendicular cruza a função
        double[] y_rp = new double[filtragem.size()];
        double[] aux = funcao.clone();
        for (int i = 0; i < filtragem.size(); i++) {
            for (int j = 0; j < aux.length; j++) {
                aux[j] = funcao[j] * Math.pow(x_rp[i], (aux.length - j - 1));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                y_rp[i] = DoubleStream.of(aux).sum();
            }
        }

        //x_rt = pontos x na reta tangente
        double pdfc = 0;
        double[] x_rt = new double[filtragem.size()];
        double[] y_rt = new double[filtragem.size()];
        double[] distancias = new double[filtragem.size()];

        /**/

        double x = c_rt - c_rp;
        for (int i = 0; i < x_rp.length; i++) {
            double constantes = (frequencias[0] - c_rt * velocidades[0]) - (y_rp[i] - c_rp * x_rp[i]);
            double[] x_contantes = new double[] {constantes, x};
            Complex64F[] raizesX_contantes = PolynomialRootFinder.findRoots(x_contantes);

            if(raizesX_contantes[0].getReal() < x_rp[i]) {
                raizesX_contantes[0].setReal(x_rp[i]);
            }
            x_rt[i] = raizesX_contantes[0].getReal();
            y_rt[i] = frequencias[0] + c_rt * (x_rt[i] - velocidades[0]);
            if(y_rt[i] > y_rp[i]) {
                y_rt[i] = y_rp[i];
            }

            double y_dif = Math.pow((y_rp[i] - y_rt[i]), 2);
            double x_dif = Math.pow((x_rp[i] - x_rt[i]), 2);
            distancias[i] = Math.sqrt(x_dif + y_dif);

            ArrayList<Double> distanciaLista = new ArrayList<>();
            for (double d : distancias) {
                distanciaLista.add(d);
            }

            double maior = Collections.max(distanciaLista);
            pdfc = x_rp[distanciaLista.indexOf(maior)];

            double resto = pdfc % 1;
            if (resto < 0.25) {
                pdfc = pdfc - resto;
            } else {
                if (resto > 0.75) {
                    pdfc = pdfc - resto + 1;
                } else {
                    pdfc = pdfc - resto + 0.5;
                }
            }

        }

        Log.e(TAG, "Resultado: Indivíduo: " + individuo);
        Log.e(TAG, "Resultado: pdfc: " + pdfc);

        return "Indivíduo: " + individuo + "\n" +
                "pdfc: " + pdfc;


        /**/

        /*
        for (int i = (derivadaMatrizCoeficientes.length - 1); i >= 0; i--) {
            derivadaMatrizCoeficientes[i] = funcao[i] * (funcao.length - (i + 1));
        }


        for (int i = 0; i < y_rp.length; i++) {
            double[] resultante = new double[filtragem.size()];
            resultante[i] = c_rt - c_rp;
            if ((i + 1) < y_rp.length) {
                resultante[i + 1] = c_rt - c_rp;
            } else {
                resultante[i] = frequencias[0] + c_rt * (-velocidades[0]) - (y_rp[i] + c_rp * (-x_rp[i]));
            }

            double[] resultanteInvertido = new double[resultante.length];
            for (int j = 0; j < resultanteInvertido.length / 2; j++) {
                double temp = resultante[j];
                resultanteInvertido[j] = resultante[resultanteInvertido.length - j - 1];
                resultanteInvertido[resultanteInvertido.length - j - 1] = temp;
            }

            //Complex64F[] raizesResultante = PolynomialRootFinder.findRoots(resultanteInvertido);
            Complex64F[] raizesResultante = PolynomialRootFinder.findRoots(resultante);
            ArrayList<Double> filtragemResultante = new ArrayList<>();
            for (Complex64F valor : raizesResultante) {
                if (valor.isReal() &&
                        valor.getReal() > x_rp[i]) {
                    filtragemResultante.add(valor.getReal());
                }
            }

            double maior;
            try {
                maior = Collections.max(filtragemResultante);
            } catch (Exception ex) {
                Log.d(TAG, "Resultado: Não há raiz real de resultante");
                return "Individuo: " + individuo + "\n" +
                        "Resultado: Não há raiz real de resultante";
            }
            x_rt[i] = maior;
        }

        //y_rt = pontos y na reta tangente
        double[] y_rtk = new double[filtragem.size()];
        for (int i = 0; i < filtragem.size(); i++) {
            y_rt[i] = y_rp[i] + c_rp * (x_rt[i] - x_rp[i]);
        }

        //Obtendo Distância
        double[] distancia = new double[filtragem.size()];
        for (int i = 0; i < filtragem.size(); i++) {
            double y_dif = Math.pow((y_rp[i] - y_rt[i]), 2);
            double x_dif = Math.pow((x_rp[i] - x_rt[i]), 2);
            distancia[i] = Math.sqrt(y_dif + x_dif);
        }
        ArrayList<Double> distanciaLista = new ArrayList<>();
        for (double d : distancia) {
            distanciaLista.add(d);
        }

        //Obtendo PDFC (Ponto de Deflexão da Frequência Cardíaca)
        double maior = Collections.max(distanciaLista);
        double pdfc = x_rt[distanciaLista.indexOf(maior)];
        //"Arredondando" o PDFC para que este seja igual a um dos pontos coletados
        double resto = pdfc % 1;
        if (resto < 0.25) {
            pdfc = pdfc - resto;
        } else {
            if (resto > 0.75) {
                pdfc = pdfc - resto + 1;
            } else {
                pdfc = pdfc - resto + 0.5;
            }
        }

        Log.e(TAG, "Resultado: Indivíduo: " + individuo);
        Log.e(TAG, "Resultado: pdfc: " + pdfc);

        return "Indivíduo: " + individuo + "\n" +
                "pdfc: " + pdfc;
                */
    }




    private static String escreverEquacao(double[] velocidades, double[] frequencias, Matrix matrizCoeficientes, double coeficienteAngular) {

        StringBuilder equacao = new StringBuilder();
        equacao.append("(");

        for (int i = 0; i < matrizCoeficientes.getArray().length; i++) {

            double[] resultados = matrizCoeficientes.getArray()[i];

            for (int j = 0; j < matrizCoeficientes.getArray()[0].length; j++) {

                if (j != matrizCoeficientes.getArray()[0].length)
                    equacao.append("(").append(resultados[j]).append("*x^").append(resultados.length - (j + 1)).append(") + ");
                else
                    equacao.append("(").append(resultados[j]).append("*x^").append(resultados.length - (j + 1)).append(")");

                //resultados[i]*x^(resultados.length-1)+resultados[i-1]*x^(resultados.length-2)+
            }
        }

        equacao.append(")");

        //matrizCoeficientes[primeiro valor(acho que é 143)]+coeficienteAngular*(x-matrizVelocidades[primeiro valor (12,5)])
        equacao.append(" -  ((").append(frequencias[0]).append(" + ").append(coeficienteAngular).append(") * (x - ").append(velocidades[0]).append("))");

        return equacao.toString();
    }
}