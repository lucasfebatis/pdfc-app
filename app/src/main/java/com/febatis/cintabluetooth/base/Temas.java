package com.febatis.cintabluetooth.base;

/**
 * Criado por lfeba em 26/05/2018.
 */
public final class Temas {

    public final static String PADRAO_ANDROID = "PADRAO_ANDROID";
    public final static String PADRAO_ANDROID_EXIBICAO = "Padrão Android";

    public final static String PDFC_CLARO = "PDFC_CLARO";
    public final static String PDFC_CLARO_EXIBICAO = "Claro";

    public final static String PDFC_ESCURO = "PDFC_ESCURO";
    public final static String PDFC_ESCURO_EXIBICAO = "Escuro";


}
