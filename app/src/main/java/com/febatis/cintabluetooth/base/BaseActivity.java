package com.febatis.cintabluetooth.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.febatis.cintabluetooth.R;
import com.febatis.cintabluetooth.util.GerenciadorSP;

import java.util.Objects;

public class BaseActivity extends AppCompatActivity {

    RelativeLayout mainLayout;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(obterTema());
        super.setContentView(R.layout.activity_base);

        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(findViewById(R.id.toolbar));
    }

    @Override
    public void setContentView(@LayoutRes int view) {

        if(view != R.layout.activity_main) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
        }

        mainLayout = findViewById(R.id.main_layout);
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Objects.requireNonNull(inflater).inflate(view, mainLayout, true);

    }

    public void mostrarSnackBar(String msg, int snackTime) {
        Snackbar.make(findViewById(R.id.root), msg, snackTime).show();
    }

    public int obterTema() {

        String tema = GerenciadorSP.obterTema(getApplicationContext());

        switch (tema) {
            case Temas.PADRAO_ANDROID:
                return R.style.AppTheme;
            case Temas.PDFC_CLARO:
                return R.style.AppTheme_PDFC_Claro;
            case Temas.PDFC_ESCURO:
                return R.style.AppTheme_PDFC_Escuro;
            default:
                return R.style.AppTheme_PDFC_Claro;
        }

    }

    public void alterarTema(String tema) {
        GerenciadorSP.definirTema(getApplicationContext(), tema);
    }

    public void setFloatButtonListener(View.OnClickListener listener) {

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(listener);


    }
}
