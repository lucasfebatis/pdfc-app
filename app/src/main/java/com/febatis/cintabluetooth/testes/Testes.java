package com.febatis.cintabluetooth.testes;

import com.febatis.cintabluetooth.matematica.JamaTestes2;

import java.util.ArrayList;

/**
 * Criado por lfeba em 15/04/2018.
 */

public final class Testes {

    public static ArrayList<String> gerarTestes() {

        ArrayList<String> retorno = new ArrayList<>();

        retorno.add(JamaTestes2.gerarTeste(Mathias.class.getSimpleName(), Mathias.velocidades, Mathias.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Gustavo.class.getSimpleName(), Gustavo.velocidades, Gustavo.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Felipe.class.getSimpleName(), Felipe.velocidades, Felipe.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Batista.class.getSimpleName(), Batista.velocidades, Batista.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Diogo.class.getSimpleName(), Diogo.velocidades, Diogo.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Richie.class.getSimpleName(), Richie.velocidades, Richie.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Larissa.class.getSimpleName(), Larissa.velocidades, Larissa.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Arthur.class.getSimpleName(), Arthur.velocidades, Arthur.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Rafael.class.getSimpleName(), Rafael.velocidades, Rafael.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Rodrigo.class.getSimpleName(), Rodrigo.velocidades, Rodrigo.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Cesar.class.getSimpleName(), Cesar.velocidades, Cesar.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Mauricio.class.getSimpleName(), Mauricio.velocidades, Mauricio.frequencias));
        retorno.add(JamaTestes2.gerarTeste(Vanessa.class.getSimpleName(), Vanessa.velocidades, Vanessa.frequencias));

        return retorno;
    }

    public static class Mathias {
        public static double[] velocidades = {6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10};
        public static double[] frequencias = {108, 116, 132, 154, 171, 176, 185, 192, 191};
    }
    
    public static class Gustavo {
        public static double[] velocidades = {6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10};
        public static double[] frequencias = {156, 162, 172, 176, 189, 192, 193, 197, 200};
    }
    
    public static class Felipe {
        public static double[] velocidades = {9, 9.5, 10, 10.5, 11, 11.5, 12};
        public static double[] frequencias = {173, 182, 191, 199, 205, 212, 213};
    }

    public static class Batista {
        public static double[] velocidades = {10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14, 14.5, 15, 15.5, 16};
        public static double[] frequencias = {142, 149, 158, 160, 163, 166, 173, 178, 183, 185, 190, 191};
    }

    public static class Diogo {
        public static double[] velocidades = {11.5, 12, 12.5, 13, 13.5, 14, 14.5, 15, 15.5, 16, 16.5};
        public static double[] frequencias = {145, 149, 154, 155, 156, 164, 166, 169, 168, 172, 174};
    }

    public static class Richie {
        public static double[] velocidades = {7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5};
        public static double[] frequencias = {134, 141, 150, 149, 157, 159, 165, 167, 172, 177, 179, 183};
    }

    public static class Larissa {
        public static double[] velocidades = {7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14, 14.5, 15};
        public static double[] frequencias = {131, 143, 145, 148, 154, 163, 166, 169, 170, 175, 181, 185, 189, 191, 193, 195};
    }

    public static class Arthur {
        public static double[] velocidades = {9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5};
        public static double[] frequencias = {157, 165, 173, 177, 182, 187, 191, 195};
    }

    public static class  Rafael {
        public static double[] velocidades = {9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14, 14.5};
        public static double[] frequencias = {136, 149, 152, 158, 160, 165, 173, 176, 181, 186, 189, 193};
    }

    public static class Rodrigo {
        public static double[] velocidades = {9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5};
        public static double[] frequencias = {126, 133, 145, 149, 154, 159, 166, 170};
    }

    public static class Cesar {
        public static double[] velocidades = {8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14, 14.5, 15};
        public static double[] frequencias = {142, 147, 140, 146, 146, 158, 156, 157, 159, 162, 170, 172, 176, 178, 180};
    }

    public static class Mauricio {
        public static double[] velocidades = {12.5, 13, 13.5, 14, 14.5, 15, 15.5, 16, 16.5, 17};
        public static double[] frequencias = {143, 151, 154, 159, 162, 167, 173, 174, 176, 176};
    }

    public static class Vanessa {
        public static double[] frequencias = {131, 140, 153, 166, 180, 186};
        public static double[] velocidades = {5, 5.5, 6, 6.5, 7, 7.5};
    }
}
