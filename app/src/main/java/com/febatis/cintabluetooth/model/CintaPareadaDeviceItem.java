package com.febatis.cintabluetooth.model;

public class CintaPareadaDeviceItem {

    private String identificador;
    private String enderecoMac;
    private String status;


    public CintaPareadaDeviceItem(String identificador, String enderecoMac, String status) {
        this.identificador = identificador;
        this.enderecoMac = enderecoMac;
        this.status = status;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getEnderecoMac() {
        return enderecoMac;
    }

    public void setEnderecoMac(String enderecoMac) {
        this.enderecoMac = enderecoMac;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
