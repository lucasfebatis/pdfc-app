package com.febatis.cintabluetooth.model;

import android.support.annotation.IdRes;

/**
 * Criado por lfeba em 20/05/2018.
 */
public class MenuItem {

    @IdRes
    private int idResource;
    private String titulo;
    private String descricao;

    public MenuItem() {
    }

    public MenuItem(int idResource, String titulo, String descricao) {
        this.idResource = idResource;
        this.titulo = titulo;
        this.descricao = descricao;
    }

    public int getIdResource() {
        return idResource;
    }

    public void setIdResource(int idResource) {
        this.idResource = idResource;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
