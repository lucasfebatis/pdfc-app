package com.febatis.cintabluetooth.model;

import com.febatis.cintabluetooth.realm.Pacote;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class HistoricoItem extends RealmObject {

    @PrimaryKey
    public long id;

    private String nome;
    private String kmPercorrido;
    private String pdfc;
    private long data;

    private RealmList<Double> freq;
    private RealmList<Double> velo;

    public HistoricoItem() {
    }

    public HistoricoItem(Pacote atributos) {
        id = Long.valueOf(String.valueOf(atributos.getObjects()[0]));
        nome = String.valueOf(atributos.getObjects()[1]);
        kmPercorrido = String.valueOf(atributos.getObjects()[2]);
        pdfc = String.valueOf(atributos.getObjects()[3]);
        data = Long.valueOf(String.valueOf(atributos.getObjects()[4]));

        setFreq((double[]) atributos.getObjects()[5]);
        setVelo((double[]) atributos.getObjects()[6]);
    }

    public HistoricoItem(String nome, String kmPercorrido, String pdfc, long data, double[] velo, double[] freq) {
        this.nome = nome;
        this.kmPercorrido = kmPercorrido;
        this.pdfc = pdfc;
        this.data = data;

        this.velo = new RealmList<>();
        this.freq = new RealmList<>();

        for(int i = 0; i < velo.length; i++) {
            this.velo.add(velo[i]);
            this.freq.add(freq[i]);
        }
    }

    @Override
    public String toString() {

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("id", id);
            jsonObj.put("nome", nome);
            jsonObj.put("kmPercorrido", kmPercorrido);
            jsonObj.put("pdfc", pdfc);
            jsonObj.put("data", data);
            jsonObj.put("freq", freq);
            jsonObj.put("velo", velo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObj.toString();

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getKmPercorrido() {
        return kmPercorrido;
    }

    public void setKmPercorrido(String kmPercorrido) {
        this.kmPercorrido = kmPercorrido;
    }

    public String getPdfc() {
        return pdfc;
    }

    public void setPdfc(String pdfc) {
        this.pdfc = pdfc;
    }

    public long getData() {
        return data;
    }

    public void setData(long data) {
        this.data = data;
    }

    @SuppressWarnings("ConstantConditions")
    public double[] getFreq() {

        if(freq == null || freq.isEmpty()) {
            return null;
        }

        double[] getFreq = new double[freq.size()];

        for(int i = 0; i < getFreq.length; i++) {
            getFreq[i] = freq.get(i);
        }

        return  getFreq;
    }

    public void setFreq(double[] freq) {
        this.freq = new RealmList<>();

        for (double aFreq : freq) {
            this.freq.add(aFreq);
        }
    }

    @SuppressWarnings("ConstantConditions")
    public double[] getVelo() {
        if(velo == null || velo.isEmpty()) {
            return null;
        }

        double[] getVelo = new double[velo.size()];

        for(int i = 0; i < getVelo.length; i++) {
            getVelo[i] = velo.get(i);
        }

        return  getVelo;
    }

    public void setVelo(double[] velo) {
        this.velo = new RealmList<>();

        for (double aVelo : velo) {
            this.velo.add(aVelo);
        }
    }


}
