package com.febatis.cintabluetooth.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.febatis.cintabluetooth.realm.Pacote;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Criado por lfeba em 28/05/2018.
 */
public class Usuario extends RealmObject implements Parcelable {

    @PrimaryKey
    public long id;
    private String nome;
    private int idade;
    private String sexo;
    private double peso;
    private double altura;

    //Dados do pré teste
    private String distancia;
    private String tempo;

    public Usuario() {

    }

    public Usuario(Pacote atributos) {
        id = Long.valueOf(String.valueOf(atributos.getObjects()[0]));
        nome = String.valueOf(atributos.getObjects()[1]);
        idade = Integer.valueOf(String.valueOf(atributos.getObjects()[2]));
        sexo = String.valueOf(atributos.getObjects()[3]);
        peso = Double.valueOf(String.valueOf(atributos.getObjects()[4]));
        altura = Double.valueOf(String.valueOf(atributos.getObjects()[5]));
        distancia = String.valueOf(atributos.getObjects()[6]);
        tempo = String.valueOf(atributos.getObjects()[7]);
    }

    public Usuario(long id, String nome, int idade, String sexo, double peso, double altura, String distancia, String tempo) {
        this.id = id;
        this.nome = nome;
        this.idade = idade;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
        this.distancia = distancia;
        this.tempo = tempo;
    }

    @Override
    public String toString() {

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("id", id);
            jsonObj.put("nome", nome);
            jsonObj.put("idade", idade);
            jsonObj.put("sexo", sexo);
            jsonObj.put("peso", peso);
            jsonObj.put("altura", altura);
            jsonObj.put("distancia", distancia);
            jsonObj.put("tempo", tempo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObj.toString();

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public String getTempo() {
        return tempo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }

    //Parcelable
    public static final Parcelable.Creator<Usuario> CREATOR = new Parcelable.Creator<Usuario>() {
        @Override
        public Usuario createFromParcel(Parcel in) {
            return new Usuario(in);
        }

        @Override
        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };

    private Usuario(Parcel in) {
        this.id = in.readLong();
        this.nome = in.readString();
        this.idade = in.readInt();
        this.sexo = in.readString();
        this.peso = in.readDouble();
        this.altura = in.readDouble();
        this.distancia = in.readString();
        this.tempo = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeLong(id);
        parcel.writeString(nome);
        parcel.writeInt(idade);
        parcel.writeString(sexo);
        parcel.writeDouble(peso);
        parcel.writeDouble(altura);
        parcel.writeString(distancia);
        parcel.writeString(tempo);

    }
}
