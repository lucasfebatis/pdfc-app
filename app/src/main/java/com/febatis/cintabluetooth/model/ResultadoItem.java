package com.febatis.cintabluetooth.model;

public class ResultadoItem {


    private String valor;

    public ResultadoItem(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
