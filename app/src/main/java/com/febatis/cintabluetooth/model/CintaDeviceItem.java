package com.febatis.cintabluetooth.model;

public class CintaDeviceItem {

    private String identificador;
    private String enderecoMac;

    public CintaDeviceItem(String identificador, String enderecoMac) {
        this.identificador = identificador;
        this.enderecoMac = enderecoMac;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getEnderecoMac() {
        return enderecoMac;
    }

    public void setEnderecoMac(String enderecoMac) {
        this.enderecoMac = enderecoMac;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof CintaDeviceItem && ((CintaDeviceItem) obj).getIdentificador().equals(identificador) && ((CintaDeviceItem) obj).getEnderecoMac().equals(enderecoMac);
    }
}
