package com.febatis.cintabluetooth.model;

public class BluetoothDeviceItem {

    private String identificador;
    private String status;

    public BluetoothDeviceItem(String identificador, String status) {
        this.identificador = identificador;
        this.status = status;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
